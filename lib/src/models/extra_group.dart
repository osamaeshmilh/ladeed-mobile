import '../helpers/custom_trace.dart';

class ExtraGroup {
  String id;
  String name;
  bool isMultiSelect;

  ExtraGroup();

  ExtraGroup.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      isMultiSelect = jsonMap['is_multi_select'];
    } catch (e) {
      id = '';
      name = '';
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    return map;
  }

  @override
  String toString() {
    return 'ExtraGroup{id: $id, name: $name, isMultiSelect: $isMultiSelect}';
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => this.id.hashCode;
}
