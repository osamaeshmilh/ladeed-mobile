import '../helpers/custom_trace.dart';

class City {
  String id;
  String name;

  City();

  City.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'].toString();
    } catch (e) {
      id = '';
      name = '';
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  City.init();
}
