import '../helpers/custom_trace.dart';
import 'city.dart';

class Zone {
  String id;
  String name;
  City city;

  Zone();

  Zone.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'].toString();
      city = jsonMap['city'] != null
          ? City.fromJSON(jsonMap['city'])
          : new City.init();
    } catch (e) {
      id = '';
      name = '';
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  Zone.init();
}
