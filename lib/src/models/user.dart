import '../helpers/custom_trace.dart';
import '../models/media.dart';

class User {
  String id;
  String name;
  String email;
  String password;
  String apiToken;
  String deviceToken;
  String phone;
  String mobile;
  String address;
  String bio;
  Media image;

  // used for indicate if client logged in or not
  bool auth;

  String social_token;

//  String role;

  User();

  User.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'] != null ? jsonMap['name'] : '';
      email = jsonMap['email'] != null ? jsonMap['email'] : '';
      mobile = jsonMap['mobile'] != null ? jsonMap['mobile'] : '';
      apiToken = jsonMap['api_token'];
      deviceToken = jsonMap['device_token'];
      try {
        phone = jsonMap['custom_fields']['phone']['view'];
      } catch (e) {
        phone = "";
      }
      try {
        address = jsonMap['custom_fields']['address']['view'];
      } catch (e) {
        address = "";
      }
      try {
        bio = jsonMap['custom_fields']['bio']['view'];
      } catch (e) {
        bio = "";
      }
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0 ? Media.fromJSON(jsonMap['media'][0]) : new Media();

      social_token = jsonMap['social_token'];
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["password"] = password;
    map["api_token"] = apiToken;
    if (deviceToken != null) {
      map["device_token"] = deviceToken;
    }
    map["phone"] = phone;
    map["address"] = address;
    map["bio"] = bio;
    map["media"] = image?.toMap();
    map["mobile"] = mobile;

    map["social_token"] = social_token;

    return map;
  }

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, password: $password, apiToken: $apiToken, deviceToken: $deviceToken, phone: $phone, mobile: $mobile, address: $address, bio: $bio, image: $image, auth: $auth, social_token: $social_token}';
  }

  bool profileCompleted() {
    return phone != null && phone != '';
  }

  Map toPasswordMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["password"] = password;

    return map;
  }

  Map toRestrictMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["thumb"] = image?.thumb;
    map["device_token"] = deviceToken;
    return map;
  }
}
