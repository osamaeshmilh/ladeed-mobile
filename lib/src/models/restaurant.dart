import 'package:food_delivery_app/src/models/aspect.dart';

import '../helpers/custom_trace.dart';
import '../models/media.dart';
import 'user.dart';
import 'zone.dart';

class Restaurant {
  String id;
  String name;
  Media image;
  String rate;
  String address;
  String description;
  String phone;
  String mobile;
  String information;
  var deliveryFee;
  double adminCommission;
  double defaultTax;
  String latitude;
  String longitude;
  bool closed;
  bool availableForDelivery;
  double deliveryRange;
  double distance;

  List<User> users;
  List<Aspect> restaurantAspectsAverage;

  bool available_for_pickup;
  double minimum_order_total_for_delivery;
  String delivery_price_type;
  bool is_online_payment_avaliable;
  String status;
  bool is_featured;
  int priority;
  int rate_count;

  String englishName;
  Zone zone;

  String deliveryPriceTypeArString() {
    if (delivery_price_type == 'fixed')
      return 'ثابتة';
    else if (delivery_price_type == 'zone')
      return 'بالمنطقة';
    else if (delivery_price_type == 'distance')
      return ('بالمسافة');
    else
      return (' ');
  }

  Restaurant();

  Restaurant.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0 ? Media.fromJSON(jsonMap['media'][0]) : new Media();
      rate = jsonMap['rate'] ?? '0';
      deliveryFee = jsonMap['delivery_fee'] ?? null;

      adminCommission = jsonMap['admin_commission'] != null ? jsonMap['admin_commission'].toDouble() : 0.0;
      deliveryRange = jsonMap['delivery_range'] != null ? jsonMap['delivery_range'].toDouble() : 0.0;
      address = jsonMap['address'];
      description = jsonMap['description'];
      phone = jsonMap['phone'];
      mobile = jsonMap['mobile'];
      defaultTax = jsonMap['default_tax'] != null ? jsonMap['default_tax'].toDouble() : 0.0;
      information = jsonMap['information'];
      latitude = jsonMap['latitude'];
      longitude = jsonMap['longitude'];
      closed = jsonMap['closed'] ?? false;
      availableForDelivery = jsonMap['available_for_delivery'] ?? false;
      distance = jsonMap['distance'] != null ? double.parse(jsonMap['distance'].toString()) : 0.0;

      available_for_pickup = jsonMap['available_for_pickup'] ?? false;
      minimum_order_total_for_delivery = jsonMap['minimum_order_total_for_delivery'] != null ? jsonMap['minimum_order_total_for_delivery'].toDouble() : 0.0;
      delivery_price_type = jsonMap['delivery_price_type'];
      is_online_payment_avaliable = jsonMap['is_online_payment_avaliable'] ?? false;
      status = jsonMap['status'];
      is_featured = jsonMap['is_featured'] ?? false;
      priority = jsonMap['priority'] ?? 0;
      rate_count = jsonMap['rate_count'] ?? 0;
      users = jsonMap['users'] != null && (jsonMap['users'] as List).length > 0 ? List.from(jsonMap['users']).map((element) => User.fromJSON(element)).toSet().toList() : [];

      restaurantAspectsAverage = jsonMap['restaurant_aspects_average'] != null && (jsonMap['restaurant_aspects_average'] as List).length > 0
          ? List.from(jsonMap['restaurant_aspects_average']).map((element) => Aspect.fromJSON(element)).toSet().toList()
          : [];
    } catch (e) {
      id = '';
      name = '';
      image = new Media();
      rate = '0';
      deliveryFee = 0.0;
      adminCommission = 0.0;
      deliveryRange = 0.0;
      address = '';
      description = '';
      phone = '';
      mobile = '';
      defaultTax = 0.0;
      information = '';
      latitude = '0';
      longitude = '0';
      closed = false;
      availableForDelivery = false;
      distance = 0.0;

      available_for_pickup = false;
      minimum_order_total_for_delivery = 0.0;
      delivery_price_type = '';
      is_online_payment_avaliable = false;
      status = '';
      is_featured = false;
      priority = 0;
      rate_count = 0;
      users = [];

      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
      'delivery_fee': deliveryFee,
      'distance': distance,
    };
  }

  @override
  String toString() {
    return 'Restaurant{id: $id, name: $name, image: $image, rate: $rate, address: $address, description: $description, phone: $phone, mobile: $mobile, information: $information, deliveryFee: $deliveryFee, adminCommission: $adminCommission, defaultTax: $defaultTax, latitude: $latitude, longitude: $longitude, closed: $closed, availableForDelivery: $availableForDelivery, deliveryRange: $deliveryRange, distance: $distance, available_for_pickup: $available_for_pickup, minimum_order_total_for_delivery: $minimum_order_total_for_delivery, delivery_price_type: $delivery_price_type, is_online_payment_avaliable: $is_online_payment_avaliable, status: $status, is_featured: $is_featured, priority: $priority}';
  }
}
