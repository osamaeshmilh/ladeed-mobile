import '../helpers/custom_trace.dart';

class Aspect {
  String id;
  String name;
  String rate;
  String identifier;
  String restaurant_review_id;
  String average;

  Aspect();

  Aspect.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString() ?? '';
      name = jsonMap['name'].toString() ?? '';
      average = jsonMap['average'].toString() ?? '';
      identifier = jsonMap['identifier'].toString() ?? '';
      rate = jsonMap['rate'].toString() ?? '0';
    } catch (e) {
      id = '';
      name = '';
      rate = '';
      restaurant_review_id = '';
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["identifier"] = identifier;
    map["rate"] = rate;
    map["restaurant_review_id"] = restaurant_review_id;
    return map;
  }

  double getTotalRate() {
    return 0.0;
  }
}
