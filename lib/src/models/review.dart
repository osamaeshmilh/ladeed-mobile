import '../helpers/custom_trace.dart';
import '../models/food.dart';
import '../models/restaurant.dart';
import '../models/user.dart';
import 'aspect.dart';
import 'media.dart';

class Review {
  String id;
  String review;
  String rate;
  User user;
  DateTime createdAt;
  List<Aspect> aspects;
  Media image;
  String averageRating;
  bool isEdited;

  Review();

  Review.init(this.rate);

  Review.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      review = jsonMap['review'];
      rate = jsonMap['rate'].toString() ?? '0';
      user = jsonMap['user'] != null ? User.fromJSON(jsonMap['user']) : User.fromJSON({});
      createdAt = DateTime.parse(jsonMap['created_at']);
      aspects = jsonMap['aspects'] != null ? List.from(jsonMap['aspects']).map((element) => Aspect.fromJSON(element)).toList() : [];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0 ? Media.fromJSON(jsonMap['media'][0]) : new Media();
      averageRating = jsonMap['average_rating'].toString() ?? '0.0';
      isEdited = jsonMap['is_edited'] ?? false;
    } catch (e) {
      id = '';
      review = '';
      rate = '0.0';
      averageRating = '0.0';
      isEdited = false;
      user = User.fromJSON({});
      createdAt = new DateTime(0);

      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["review"] = review;
    map["user_id"] = user?.id;
    map["aspects"] = aspects.map((element) => element.toMap()).toList();

    return map;
  }

  @override
  String toString() {
    return this.toMap().toString();
  }

  Map ofRestaurantToMap(Restaurant restaurant) {
    var map = this.toMap();
    map["restaurant_id"] = restaurant.id;
    return map;
  }

  Map ofFoodToMap(Food food) {
    var map = this.toMap();
    map["food_id"] = food.id;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => this.id.hashCode;

  bool isAspectsRated() {
    var isRated = false;
    aspects.forEach((aspect) {
      isRated = (aspect.rate == '0');
    });
    return isRated;
  }

  Review.fromJSONNoAspects(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      review = jsonMap['review'];
      rate = jsonMap['rate'].toString() ?? '0';
      user = jsonMap['user'] != null ? User.fromJSON(jsonMap['user']) : User.fromJSON({});
      createdAt = DateTime.parse(jsonMap['created_at']);
      // aspects = jsonMap['aspects'] != null ? List.from(jsonMap['aspects']).map((element) => Aspect.fromJSON(element)).toList() : [];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0 ? Media.fromJSON(jsonMap['media'][0]) : new Media();
      averageRating = jsonMap['average_rating'].toString() ?? '0.0';
      isEdited = jsonMap['is_edited'] ?? false;
    } catch (e) {
      id = '';
      review = '';
      rate = '0';
      averageRating = '0';
      isEdited = false;
      user = User.fromJSON({});
      createdAt = new DateTime(0);

      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}
