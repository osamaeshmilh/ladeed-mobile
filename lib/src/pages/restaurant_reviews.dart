import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/elements/EmptyReviewsWidget.dart';
import 'package:food_delivery_app/src/elements/ReviewsListWidget.dart';
import 'package:food_delivery_app/src/helpers/helper.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:food_delivery_app/src/repository/user_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/restaurant_controller.dart';
import '../elements/DrawerWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';
import '../models/restaurant.dart';
import '../models/route_argument.dart';

class RestaurantReviewsWidget extends StatefulWidget {
  @override
  _RestaurantReviewsWidgetState createState() => _RestaurantReviewsWidgetState();
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  RestaurantReviewsWidget({Key key, this.parentScaffoldKey, this.routeArgument}) : super(key: key);
}

class _RestaurantReviewsWidgetState extends StateMVC<RestaurantReviewsWidget> {
  RestaurantController _con;
  List<String> selectedCategories;

  _RestaurantReviewsWidgetState() : super(RestaurantController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.restaurant = widget.routeArgument.param as Restaurant;
    _con.listenForRestaurantReviews(id: _con.restaurant.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        title: Text(
          _con.restaurant?.name ?? '',
          overflow: TextOverflow.fade,
          softWrap: false,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 0)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(" " + getRatingText(context) + "  ", style: Theme.of(context).textTheme.caption.merge(TextStyle(color: getRatingColor(), fontWeight: FontWeight.bold, fontSize: 24))),
                      Text(_con.restaurant.rate, style: Theme.of(context).textTheme.caption.merge(TextStyle(color: getRatingColor(), fontWeight: FontWeight.bold, fontSize: 24))),
                      SizedBox(
                        width: 3,
                      ),
                      Icon(
                        LadeedIcons.star__1_,
                        color: getRatingColor(),
                        size: 20,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Center(
                    child: Text('عدد التقييمات ' + _con.restaurant.rate_count.toString(), textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1),
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Column(
                      children: [
                        ListView.separated(
                          padding: EdgeInsets.all(0),
                          itemBuilder: (context, aspectIndex) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  _con.restaurant.restaurantAspectsAverage.elementAt(aspectIndex).name,
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                ),
                                Row(
                                  children: [
                                    Row(
                                      children: Helper.getStarsListNoCount(double.parse(_con.restaurant.restaurantAspectsAverage.elementAt(aspectIndex).average)),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      _con.restaurant.restaurantAspectsAverage.elementAt(aspectIndex).average.substring(0, 3),
                                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                    )
                                  ],
                                )
                              ],
                            );
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox(height: 15);
                          },
                          itemCount: _con.restaurant.restaurantAspectsAverage.length,
                          primary: false,
                          shrinkWrap: true,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                title: Text(
                  S.of(context).what_they_say,
                  style: Theme.of(context).textTheme.headline4,
                ),
                trailing: OutlineButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                  ),
                  child: Text(
                    S.of(context).add_review,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                  ),
                  onPressed: () {
                    if (currentUser.value.apiToken == null) {
                      Navigator.of(context).pushNamed('/Login');
                    } else {
                      Navigator.of(context).pushReplacementNamed('/RestaurantReview', arguments: RouteArgument(id: _con.restaurant.id, heroTag: "restaurant_reviews"));
                    }
                  },
                  textColor: Theme.of(context).accentColor.withOpacity(1),
                ),
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
              color: Colors.grey,
            ),
            _con.reviews.isEmpty
                ? EmptyReviewsWidget()
                : Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: ReviewsListWidget(reviewsList: _con.reviews),
                  ),
          ],
        ),
      ),
    );
  }

  String getRatingText(context) {
    double rate = double.parse(_con.restaurant.rate);
    if (rate == 5.0)
      return S.of(context).ladeed;
    else if (rate >= 4.5 && rate <= 4.9)
      return S
          .of(context)
          .excellent;
    else if (rate >= 4.0 && rate < 4.5)
      return S
          .of(context)
          .very_good;
    else if (rate >= 3.5 && rate < 4.0)
      return S
          .of(context)
          .good;
    else if (rate >= 2.5 && rate < 3.5)
      return S
          .of(context)
          .ok;
    else if (rate >= 2.0 && rate < 2.5)
      return S
          .of(context)
          .poor;
    else
      return "";
  }

  Color getRatingColor() {
    double rate = double.parse(_con.restaurant.rate);
    if (rate == 5.0)
      return Colors.green;
    else if (rate >= 4.5 && rate <= 4.9)
      return Colors.green[400];
    else if (rate >= 4.0 && rate < 4.5)
      return Colors.lightGreen[400];
    else if (rate >= 3.5 && rate < 4.0)
      return Colors.yellow[700];
    else if (rate >= 2.5 && rate < 3.5)
      return Colors.orange;
    else if (rate >= 2.0 && rate < 2.5)
      return Colors.red;
    else
      return Colors.grey;
  }
}
