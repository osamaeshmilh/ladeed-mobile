import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class Walkthrough extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WalkthroughWidget();
  }
}

class WalkthroughWidget extends StatefulWidget {
  WalkthroughWidget({
    Key key,
  }) : super(key: key);

  @override
  _WalkthroughWidgetState createState() => _WalkthroughWidgetState();
}

class _WalkthroughWidgetState extends StateMVC<WalkthroughWidget> {
  List<Slide> slides = new List();

  @override
  void initState() {
    slides.add(
      new Slide(
        title: "مرحبا بك في تطبيق لـذيـذ",
        styleTitle: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
        styleDescription: TextStyle(color: Colors.white, fontSize: 24),
        description: "استمتع بالخدمات الرائعة التي يقدمها تطبيق لـذيـذ !",
        pathImage: "assets/img/one.png",
        backgroundColor: Colors.deepOrange,
      ),
    );
    slides.add(
      new Slide(
        title: "جميع المطاعم والأصناف",
        styleTitle: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
        styleDescription: TextStyle(color: Colors.white, fontSize: 24),
        description:
            "تصفح معنا مجموعة كبيرة من المطاعم والمنيو الخاصة بكل مطعم",
        pathImage: "assets/img/two.png",
        backgroundColor: Colors.deepOrange,
      ),
    );
    slides.add(
      new Slide(
        title: "توصيل مباشر لعندك",
        styleTitle: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
        styleDescription: TextStyle(color: Colors.white, fontSize: 24),
        description:
            "اطلب عن طريق خدمة التوصيل وتابع السائق في الخريطة لين يوصل باب حوشك!",
        pathImage: "assets/img/three.png",
        backgroundColor: Colors.deepOrange,
      ),
    );
    slides.add(
      new Slide(
        title: "ماتبيش توصيل ؟",
        styleTitle: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
        styleDescription: TextStyle(color: Colors.white, fontSize: 24),
        description:
            "التطبيق يقولك أمتا طلبيتك تكون جاهزة وتقدر تستلمها من المطعم",
        pathImage: "assets/img/four.png",
        backgroundColor: Colors.deepOrange,
      ),
    );
    super.initState();
  }

  void onDonePress() {
    Navigator.of(context).pushReplacementNamed('/Pages', arguments: 2);
    // Do what you want
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      colorActiveDot: Colors.grey.withOpacity(0.5),
      colorDot: Colors.white,
      colorDoneBtn: Colors.white.withOpacity(0.2),
      colorPrevBtn: Colors.white.withOpacity(0.2),
      colorSkipBtn: Colors.white.withOpacity(0.2),
      styleNameDoneBtn: TextStyle(color: Colors.white),
      styleNamePrevBtn: TextStyle(color: Colors.white),
      styleNameSkipBtn: TextStyle(color: Colors.white),
      nameDoneBtn: 'الانتهاء',
      nameSkipBtn: 'تخطي',
      nameNextBtn: 'التالي',
      namePrevBtn: 'رجوع',
    );
  }
}
