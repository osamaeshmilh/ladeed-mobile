import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).about_us,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/img/logo.png',
                width: 150,
                fit: BoxFit.cover,
              ),
              SizedBox(height: 50),
              Center(child: Text('تطبيق لذيذ لتوصيل الطعام', textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1)),
              SizedBox(height: 10),
              Center(child: Text('تواصل معنا عبر', textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1)),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.messenger,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("https://m.me/ladeed.ly/");
                        }),
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.facebook__1_,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("https://fb.com/ladeed.ly/");
                        }),
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.instagram,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("https://instagram.com/ladeed.ly/");
                        }),
                    IconButton(
                        icon: new Icon(
                          FontAwesomeIcons.whatsapp,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("whatsapp://send?phone=+218913652299");
                        }),
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.telephone,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("tel:+218913652299");
                        }),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        'شروط الاستخدام',
                        style: TextStyle(fontSize: 10, color: Colors.blueGrey),
                      ),
                      onPressed: () {
                        launch("https://ladeed.ly/terms-of-use/");
                      },
                    ),
                    Text('  .  '),
                    FlatButton(
                      child: Text(
                        'سياسة الخصوصية',
                        style: TextStyle(fontSize: 10, color: Colors.blueGrey),
                      ),
                      onPressed: () {
                        launch("https://ladeed.ly/privacy-policy/");
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'جميع الحقوق محفوظة © لـذيـذ ' + DateTime.now().year.toString(),
                      style: TextStyle(color: Colors.blueGrey),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
