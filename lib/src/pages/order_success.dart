import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/models/zone.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/checkout_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../helpers/helper.dart';
import '../models/payment.dart';
import '../models/route_argument.dart';

class OrderSuccessWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  final RouteArgument isDelivery;

  OrderSuccessWidget({Key key, this.routeArgument, this.isDelivery}) : super(key: key);

  @override
  _OrderSuccessWidgetState createState() => _OrderSuccessWidgetState();
}

class _OrderSuccessWidgetState extends StateMVC<OrderSuccessWidget> {
  CheckoutController _con;

  _OrderSuccessWidgetState() : super(CheckoutController()) {
    _con = controller;
  }

  @override
  void initState() {
    // route param contains the payment method
    _con.is_delivery = widget.isDelivery.param;
    _con.payment = new Payment(widget.routeArgument.param);
    _con.listenForCarts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).hintColor,
          ),
          centerTitle: true,
          title: Text(
            S.of(context).confirmation,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: _con.carts.isEmpty
            ? CircularLoadingWidget(height: 500)
            : Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    alignment: AlignmentDirectional.center,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Center(
                          child: _con.loading
                              ? CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).hintColor))
                              : Container(
                                  height: 200,
                                  width: 200,
                                  child: _con.ok
                                      ? FlareActor("assets/img/checked.flr", alignment: Alignment.center, fit: BoxFit.contain, animation: "Untitled")
                                      : FlareActor("assets/img/cancel_icon.flr", alignment: Alignment.center, fit: BoxFit.contain, animation: "Error"),
                                ),
                        ),
                        SizedBox(height: 15),
                        _con.loading
                            ? Opacity(
                                opacity: 0.4,
                                child: Text(
                                  'جاري ارسال الطلب ...',
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
                                ),
                              )
                            : Opacity(
                                opacity: 0.4,
                                child: _con.ok
                                    ? Text(
                                        S.of(context).your_order_has_been_successfully_submitted,
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
                                      )
                                    : Text(
                                        ' فشل ارسال الطلب, الرجاء التحقق من العنوان ورقم الهاتف !',
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
                                      ),
                              ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      height: 255,
                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                          boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, -2), blurRadius: 5.0)]),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    S.of(context).subtotal,
                                    style: Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                Helper.getPrice(_con.subTotal, context, style: Theme.of(context).textTheme.subtitle1)
                              ],
                            ),
                            SizedBox(height: 3),
                            _con.payment.method == 'Pay on Pickup'
                                ? SizedBox(height: 0)
                                : Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          S.of(context).delivery_fee,
                                          style: Theme.of(context).textTheme.bodyText1,
                                        ),
                                      ),
                                      if (_con.restaurant != null)
                                        if (_con.carts[0].food.restaurant.delivery_price_type == 'fixed')
                                          Helper.getPrice(_con.carts[0].food.restaurant.deliveryFee, context, style: Theme.of(context).textTheme.subtitle1)
                                        else if (_con.carts[0].food.restaurant.delivery_price_type == 'distance')
                                          Helper.getDistancePrice(_con.restaurant.deliveryFee, Helper.getDistanceKmDouble(_con.restaurant.distance), context,
                                              style: Theme.of(context).textTheme.subtitle1)
                                        else if (_con.carts[0].food.restaurant.delivery_price_type == 'zone')
                                          Helper.getZonePrice(_con.restaurant.deliveryFee, Zone(), context, style: Theme.of(context).textTheme.subtitle1)
                                        else
                                          Helper.getPrice(0, context, style: Theme.of(context).textTheme.subtitle1)
                                    ],
                                  ),
                            SizedBox(height: 3),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    "${S.of(context).tax} (${_con.carts[0].food.restaurant.defaultTax}%)",
                                    style: Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                Helper.getPrice(_con.taxAmount, context, style: Theme.of(context).textTheme.subtitle1)
                              ],
                            ),
                            Divider(height: 30),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    S.of(context).total,
                                    style: Theme.of(context).textTheme.headline6,
                                  ),
                                ),
                                Helper.getPrice(_con.is_delivery ? _con.total : _con.total - _con.deliveryFee, context, style: Theme.of(context).textTheme.headline6)
                              ],
                            ),
                            SizedBox(height: 20),
                            SizedBox(
                              width: MediaQuery.of(context).size.width - 40,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamed('/Pages', arguments: 3);
                                },
                                padding: EdgeInsets.symmetric(vertical: 14),
                                color: Theme.of(context).accentColor,
                                shape: StadiumBorder(),
                                child: Text(
                                  S.of(context).my_orders,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ));
  }
}
