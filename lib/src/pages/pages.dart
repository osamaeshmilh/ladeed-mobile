import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';

import '../../generated/l10n.dart';
import '../elements/DrawerWidget.dart';
import '../elements/FilterWidget.dart';
import '../pages/home.dart';
import '../pages/map.dart';
import '../pages/orders.dart';
import 'latest_reviews.dart';

// ignore: must_be_immutable
class PagesWidget extends StatefulWidget {
  dynamic currentTab;
  DateTime currentBackPressTime;

  PagesWidget({
    Key key,
    this.currentTab,
  }) {}

  @override
  _PagesWidgetState createState() {
    return _PagesWidgetState();
  }
}

class _PagesWidgetState extends State<PagesWidget> {
  int _selectedTab = 0;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var _pages;

  @override
  void initState() {
    _pages = [HomeWidget(parentScaffoldKey: scaffoldKey), MapWidget(parentScaffoldKey: scaffoldKey), OrdersWidget(parentScaffoldKey: scaffoldKey), LatestReviewsWidget(parentScaffoldKey: scaffoldKey)];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        key: scaffoldKey,
        drawer: DrawerWidget(),
        endDrawer: FilterWidget(onFilter: (filter) {
          Navigator.of(context).pushReplacementNamed('/Pages', arguments: widget.currentTab);
        }),
        body: IndexedStack(
          index: _selectedTab,
          children: _pages,
        ),
        bottomNavigationBar: BottomNavigationBar(
          fixedColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).primaryColor,
          currentIndex: _selectedTab,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              _selectedTab = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(LadeedIcons.home),
              label: S.of(context).home,
            ),
            BottomNavigationBarItem(
              icon: Icon(LadeedIcons.compass),
              label: S.of(context).near_to,
            ),
            BottomNavigationBarItem(
              icon: Icon(LadeedIcons.food_serving),
              label: S.of(context).my_orders,
            ),
            BottomNavigationBarItem(
              icon: Icon(LadeedIcons.favorite),
              label: S.of(context).reviews,
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (widget.currentBackPressTime == null || now.difference(widget.currentBackPressTime) > Duration(seconds: 2)) {
      widget.currentBackPressTime = now;
      Fluttertoast.showToast(msg: 'اضغط مرة اخرى للخروج');
      return Future.value(false);
    }
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return Future.value(true);
  }
}
