import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../../generated/l10n.dart';
import '../controllers/home_controller.dart';
import '../elements/CardsCarouselWidget.dart';
import '../elements/CaregoriesCarouselWidget.dart';
import '../elements/DeliveryAddressBottomSheetWidget.dart';
import '../elements/HomeSliderWidget.dart';
import '../elements/SearchBarWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';
import '../repository/settings_repository.dart' as settingsRepo;
import '../repository/user_repository.dart';

class HomeWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  HomeWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> {
  HomeController _con;
  GlobalKey keyButton = GlobalKey();

  TutorialCoachMark tutorialCoachMark;
  List<TargetFocus> targets = List();

  _HomeWidgetState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    this.initTargets();
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: ValueListenableBuilder(
          valueListenable: settingsRepo.setting,
          builder: (context, value, child) {
            return Container(
              height: 22,
              child: Image.asset('assets/img/ladeed-orange.png'),
            );
          },
        ),
//        title: Text(
//          settingsRepo.setting?.value.appName ?? S.of(context).home,
//          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
//        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshHome,
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: SearchBarWidget(
                  onClickFilter: (event) {
                    widget.parentScaffoldKey.currentState.openEndDrawer();
                  },
                ),
              ),
              HomeSliderWidget(slides: _con.slides),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  trailing: IconButton(
                    key: keyButton,
                    onPressed: () {
                      if (currentUser.value.apiToken == null) {
                        _con.requestForCurrentLocation(context);
                      } else {
                        var bottomSheetController = widget.parentScaffoldKey.currentState.showBottomSheet(
                          (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: widget.parentScaffoldKey),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                          ),
                        );
                        bottomSheetController.closed.then((value) {
                          _con.refreshHome();
                        });
                      }
                    },
                    icon: Icon(
                      LadeedIcons.go_to_location,
                      size: 28,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  title: Text(
                    S.of(context).top_restaurants,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  subtitle: Text(
                    S.of(context).near_to + " " + (settingsRepo.deliveryAddress.value?.address ?? S.of(context).unknown),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
              ),
              Divider(color: Colors.grey.shade300, indent: 16, endIndent: 16),
              CardsCarouselWidget(restaurantsList: _con.nearRestaurants, heroTag: 'home_top_restaurants'),
              // ListTile(
              //   dense: true,
              //   contentPadding: EdgeInsets.symmetric(horizontal: 20),
              //   leading: Icon(
              //     Icons.trending_up,
              //     color: Theme.of(context).hintColor,
              //   ),
              //   title: Text(
              //     S.of(context).trending_this_week,
              //     style: Theme.of(context).textTheme.headline4,
              //   ),
              //   subtitle: Text(
              //     S.of(context).clickOnTheFoodToGetMoreDetailsAboutIt,
              //     style: Theme.of(context).textTheme.caption.merge(TextStyle(fontSize: 11)),
              //   ),
              // ),
              // FoodsCarouselWidget(foodsList: _con.trendingFoods, heroTag: 'home_food_carousel'),
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 10, right: 15),
                child: ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  leading: Icon(
                    LadeedIcons.star2,
                    color: Theme.of(context).hintColor,
                  ),
                  title: Text(
                    S.of(context).most_review,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  trailing: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                    ),
                    child: Text(
                      S.of(context).view_all,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/AllRestaurants');
                    },
                    textColor: Theme.of(context).accentColor.withOpacity(1),
                  ),
                ),
              ),
              Divider(color: Colors.grey.shade300, indent: 16, endIndent: 16),
              CardsCarouselWidget(restaurantsList: _con.recentReviewedRestaurants, heroTag: 'free_delivery_restaurants'),
              // Padding(
              //   padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              //   child: ListTile(
              //     dense: true,
              //     contentPadding: EdgeInsets.symmetric(vertical: 0),
              //     title: Text(
              //       S.of(context).newly_joined,
              //       style: Theme.of(context).textTheme.headline4,
              //     ),
              //     trailing: FlatButton(
              //       shape: RoundedRectangleBorder(
              //         borderRadius: new BorderRadius.circular(18.0),
              //       ),
              //       child: Text(
              //         S.of(context).view_all,
              //         style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              //       ),
              //       onPressed: () {
              //         Navigator.of(context).push(SearchModal());
              //       },
              //       textColor: Theme.of(context).accentColor.withOpacity(1),
              //     ),
              //   ),
              // ),
              // Divider(color: Colors.grey.shade300, indent: 16, endIndent: 16),
              // CardsCarouselWidget(restaurantsList: _con.recentRestaurants, heroTag: 'new_restaurants'),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  leading: Icon(
                    LadeedIcons.fast_food,
                    size: 30,
                    color: Theme.of(context).hintColor,
                  ),
                  title: Text(
                    S
                        .of(context)
                        .food_categories,
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline4,
                  ),
                  trailing: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                    ),
                    child: Text(
                      S
                          .of(context)
                          .view_all,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/Categories');
                    },
                    textColor: Theme
                        .of(context)
                        .accentColor
                        .withOpacity(1),
                  ),
                ),
              ),
              Divider(color: Colors.grey.shade300, indent: 16, endIndent: 16),
              SizedBox(
                height: 5,
              ),
              CategoriesCarouselWidget(
                categories: _con.categories,
              ),
//              Padding(
//                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
//                child: ListTile(
//                  dense: true,
//                  contentPadding: EdgeInsets.symmetric(vertical: 0),
//                  leading: Icon(
//                    Icons.trending_up,
//                    color: Theme.of(context).hintColor,
//                  ),
//                  title: Text(
//                    S.of(context).most_popular,
//                    style: Theme.of(context).textTheme.headline4,
//                  ),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 20),
//                child: GridWidget(
//                  restaurantsList: _con.popularRestaurants,
//                  heroTag: 'home_restaurants',
//                ),
//              ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 child: ListTile(
//                   dense: true,
//                   contentPadding: EdgeInsets.symmetric(vertical: 20),
//                   leading: Icon(
//                     Icons.recent_actors,
//                     color: Theme.of(context).hintColor,
//                   ),
//                   title: Text(
//                     S.of(context).recent_reviews,
//                     style: Theme.of(context).textTheme.headline4,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 child: ReviewsListWidget(reviewsList: _con.recentReviews),
//               ),
            ],
          ),
        ),
      ),
    );
  }

  void initTargets() {
    targets.add(
      TargetFocus(
        identify: "Target 0",
        keyTarget: keyButton,
        contents: [
          ContentTarget(
              align: AlignContent.bottom,
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "حدد موقعك من هنا !",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "اضغط هنا لتحديد موقعك وعرض المطاعم الأقرب اليك.",
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }

  void showTutorial() {
    tutorialCoachMark = TutorialCoachMark(context, targets: targets,
        colorShadow: Colors.deepOrange,
        textSkip: "تخطي",
        paddingFocus: 10,
        opacityShadow: 0.8,
        onFinish: () {
          print("finish");
        },
        onClickTarget: (target) {
          print(target);
        },
        onClickSkip: () {
          print("skip");
        })
      ..show();
  }

  void _afterLayout(_) {
    Future.delayed(Duration(milliseconds: 100), () {
      settingsRepo.isLocationFirstLaunch().then((val) {
        if (val) {
          showTutorial();
        }
      });
    });
  }
}
