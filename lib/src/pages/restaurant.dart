import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:food_delivery_app/src/models/distance_price.dart';
import 'package:food_delivery_app/src/models/zone_price.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';
import '../controllers/restaurant_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/FoodItemWidget.dart';
import '../elements/GalleryCarouselWidget.dart';
import '../elements/ShoppingCartFloatButtonWidget.dart';
import '../helpers/helper.dart';
import '../models/restaurant.dart';
import '../models/route_argument.dart';
import '../repository/settings_repository.dart';

class RestaurantWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  RestaurantWidget({Key key, this.parentScaffoldKey, this.routeArgument}) : super(key: key);

  @override
  _RestaurantWidgetState createState() {
    return _RestaurantWidgetState();
  }
}

class _RestaurantWidgetState extends StateMVC<RestaurantWidget> {
  RestaurantController _con;

  _RestaurantWidgetState() : super(RestaurantController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.restaurant = widget.routeArgument.param as Restaurant;
    _con.listenForGalleries(_con.restaurant.id);
    _con.listenForFeaturedFoods(_con.restaurant.id);
    _con.listenForRestaurantReviews(id: _con.restaurant.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        body: RefreshIndicator(
          onRefresh: _con.refreshRestaurant,
          child: _con.restaurant == null
              ? CircularLoadingWidget(height: 500)
              : Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    CustomScrollView(
                      primary: true,
                      shrinkWrap: false,
                      slivers: <Widget>[
                        SliverAppBar(
                          backgroundColor: Theme.of(context).accentColor.withOpacity(0.9),
                          expandedHeight: 240,
                          elevation: 0,
//                          iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
                          automaticallyImplyLeading: false,
                          leading: new IconButton(
                            icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
                            onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
                          ),
                          flexibleSpace: FlexibleSpaceBar(
                            collapseMode: CollapseMode.parallax,
                            background: Stack(
                              fit: StackFit.expand,
                              children: [
                                Hero(
                                  tag: (widget?.routeArgument?.heroTag ?? '') + _con.restaurant.id,
                                  child: CachedNetworkImage(
                                    fit: BoxFit.cover,
                                    imageUrl: _con.restaurant.image.url,
                                    placeholder: (context, url) => Image.asset(
                                      'assets/img/loading.gif',
                                      fit: BoxFit.cover,
                                    ),
                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                                        decoration: BoxDecoration(color: _con.restaurant.closed ? Colors.grey : Colors.green, borderRadius: BorderRadius.circular(24)),
                                        child: _con.restaurant.closed
                                            ? Text(
                                                S.of(context).closed,
                                                style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                              )
                                            : Text(
                                                S.of(context).open,
                                                style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                              ),
                                      ),
                                      Helper.canDelivery(_con.restaurant)
                                          ? Container(
                                              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                                              padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                                              decoration: BoxDecoration(color: Helper.canDelivery(_con.restaurant) ? Colors.green : Colors.transparent, borderRadius: BorderRadius.circular(24)),
                                              child: Helper.canDelivery(_con.restaurant)
                                                  ? Text(
                                                      S.of(context).delivery,
                                                      style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                                    )
                                                  : Text(''),
                                            )
                                          : Text(''),
                                      Helper.canDelivery(_con.restaurant) ? SizedBox(width: 10) : SizedBox(width: 0),
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                                        decoration: BoxDecoration(color: _con.restaurant.available_for_pickup ? Colors.green : Colors.transparent, borderRadius: BorderRadius.circular(24)),
                                        child: _con.restaurant.available_for_pickup
                                            ? Text(
                                                'استلام',
                                                style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                              )
                                            : Text(''),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Wrap(
                            children: [
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 15, left: 15, bottom: 10, top: 15),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: [
                                              Text(
                                                _con.restaurant?.name ?? '',
                                                overflow: TextOverflow.fade,
                                                softWrap: false,
                                                maxLines: 2,
                                                style: Theme.of(context).textTheme.headline3,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 8.0),
                                                child: Row(
                                                  children: Helper.getStarsList(double.parse(_con.restaurant.rate), _con.restaurant.rate_count),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 32,
                                          child: Chip(
                                            padding: EdgeInsets.all(0),
                                            label: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(_con.restaurant.rate, style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColor))),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Icon(
                                                  LadeedIcons.star__1_,
                                                  color: Theme.of(context).primaryColor,
                                                  size: 14,
                                                ),
                                              ],
                                            ),
                                            backgroundColor: Theme.of(context).accentColor.withOpacity(0.9),
                                            shape: StadiumBorder(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  _con.restaurant.information != null
                                      ? Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                                          child: Helper.applyHtml(context, _con.restaurant.information),
                                        )
                                      : SizedBox(),
                                  ImageThumbCarouselWidget(galleriesList: _con.galleries),
                                  Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                    margin: const EdgeInsets.symmetric(vertical: 5),
                                    color: Theme.of(context).primaryColor,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: [
                                              Text(
                                                _con.restaurant.address ?? '',
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context).textTheme.bodyText1,
                                              ),
                                              Text(
                                                Helper.getDistance(_con.restaurant.distance, Helper.of(context).trans(setting.value.distanceUnit)),
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .caption,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          width: 32,
                                          height: 32,
                                          child: IconButton(
                                            padding: EdgeInsets.all(0),
                                            iconSize: 20,
                                            icon: new Image.asset('assets/img/google-maps.png'),
                                            color: Theme
                                                .of(context)
                                                .accentColor,
                                            onPressed: () {
                                              launch('https://www.google.com/maps/search/?api=1&query=' + _con.restaurant.latitude.toString() + ',' + _con.restaurant.longitude.toString());
                                            },
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          width: 32,
                                          height: 32,
                                          child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              Navigator.of(context).pushNamed('/Pages', arguments: new RouteArgument(id: '1', param: _con.restaurant));
                                            },
                                            child: Icon(
                                              Icons.directions,
                                              color: Theme
                                                  .of(context)
                                                  .primaryColor,
                                              size: 20,
                                            ),
                                            color: Theme
                                                .of(context)
                                                .accentColor
                                                .withOpacity(0.9),
                                            shape: StadiumBorder(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  _con.restaurant.phone != null
                                      ? Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                    margin: const EdgeInsets.symmetric(vertical: 5),
                                    color: Theme
                                        .of(context)
                                        .primaryColor,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 6.0),
                                            child: Text(
                                              '${_con.restaurant.phone}',
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .bodyText1,
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          width: 32,
                                          height: 32,
                                          child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              launch("tel:${_con.restaurant.phone}");
                                            },
                                            child: Icon(
                                              Icons.call,
                                              color: Theme
                                                  .of(context)
                                                  .primaryColor,
                                              size: 20,
                                            ),
                                            color: Theme
                                                .of(context)
                                                .accentColor
                                                .withOpacity(0.9),
                                            shape: StadiumBorder(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                      : SizedBox(),
                                  _con.restaurant.mobile != null
                                      ? Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                    margin: const EdgeInsets.symmetric(vertical: 5),
                                    color: Theme
                                        .of(context)
                                        .primaryColor,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 6.0),
                                            child: Text(
                                              '${_con.restaurant.mobile}',
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .bodyText1,
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          width: 32,
                                          height: 32,
                                          child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              launch("tel:${_con.restaurant.mobile}");
                                            },
                                            child: Icon(
                                              Icons.call,
                                              color: Theme
                                                  .of(context)
                                                  .primaryColor,
                                              size: 20,
                                            ),
                                            color: Theme
                                                .of(context)
                                                .accentColor
                                                .withOpacity(0.9),
                                            shape: StadiumBorder(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                      : SizedBox(),
                                  _con.featuredFoods.isEmpty
                                      ? SizedBox(height: 0)
                                      : Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 15),
                                    child: ListTile(
                                      dense: true,
                                      contentPadding: EdgeInsets.symmetric(vertical: 0),
                                      leading: Icon(
                                        Icons.restaurant,
                                        color: Theme
                                            .of(context)
                                            .hintColor,
                                      ),
                                      title: Text(
                                        S
                                            .of(context)
                                            .featured_foods,
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .headline4,
                                      ),
                                    ),
                                  ),
                                  _con.featuredFoods.isEmpty
                                      ? SizedBox(height: 0)
                                      : ListView.separated(
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    primary: false,
                                    itemCount: _con.featuredFoods.length,
                                    separatorBuilder: (context, index) {
                                      return SizedBox(height: 10);
                                    },
                                    itemBuilder: (context, index) {
                                      return FoodItemWidget(
                                        heroTag: 'details_featured_food',
                                        food: _con.featuredFoods.elementAt(index),
                                      );
                                    },
                                  ),
                                  Container(
                                    color: Colors.white,
                                    child: ExpansionTile(
                                      backgroundColor: Colors.white,
                                      leading: Icon(LadeedIcons.delivery_man),
                                      title: Text(S
                                          .of(context)
                                          .delivery_prices),
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                          color: Theme
                                              .of(context)
                                              .primaryColor,
                                          child: ListTile(
                                            title: Text(S
                                                .of(context)
                                                .delivery_pricing_method),
                                            trailing: Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                color: Colors.green,
                                                borderRadius: BorderRadius.circular(50),
                                              ),
                                              child: Text(
                                                _con.restaurant.deliveryPriceTypeArString() ?? ' ',
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .headline4
                                                    .merge(TextStyle(color: Theme
                                                    .of(context)
                                                    .primaryColor)),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Divider(color: Colors.grey.shade300, indent: 16, endIndent: 16),
                                        _con.restaurant.delivery_price_type == 'fixed'
                                            ? Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                          margin: const EdgeInsets.symmetric(vertical: 5),
                                          color: Theme
                                              .of(context)
                                              .primaryColor,
                                          child: ListTile(
                                            title: Text('سعر التوصيل'),
                                            trailing: Text(
                                              _con.restaurant.deliveryFee.toString() + ' دينار ' ?? ' ',
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .headline4,
                                            ),
                                          ),
                                        )
                                            : Container(),
                                        _con.restaurant.delivery_price_type == 'distance' && _con.restaurant.deliveryFee != null
                                            ? getDistancePrices(List.from(_con.restaurant.deliveryFee).map((element) => DistancePrice.fromJSON(element)).toSet().toList())
                                            : Container(),
                                        _con.restaurant.delivery_price_type == 'zone' && _con.restaurant.deliveryFee != null
                                            ? getZonePrices(List.from(_con.restaurant.deliveryFee).map((element) => ZonePrice.fromJSON(element)).toSet().toList())
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    color: Colors.white,
                                    child: ExpansionTile(
                                      backgroundColor: Colors.white,
                                      leading: Icon(LadeedIcons.minute),
                                      title: Text(S
                                          .of(context)
                                          .working_hours),
                                      children: [],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 32,
                      left: 20,
                      child: ShoppingCartFloatButtonWidget(
                          iconColor: Theme.of(context).primaryColor, labelColor: Theme.of(context).hintColor, routeArgument: RouteArgument(id: '0', param: _con.restaurant.id, heroTag: 'home_slide')),
                    ),
                  ],
                ),
        ));
  }

  Widget getDistancePrices(List<DistancePrice> distancePrices) {
    return Column(
        children: distancePrices.map((DistancePrice distancePrice) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        color: Theme
            .of(context)
            .primaryColor,
        child: ListTile(
          title: Text(distancePrice.from.toString() + ' - ' + distancePrice.to.toString() + ' كم '),
          trailing: Text(
            distancePrice.price.toString() + ' دينار ' ?? ' ',
            style: Theme
                .of(context)
                .textTheme
                .headline4,
          ),
        ),
      );
    }).toList());
  }

  Widget getZonePrices(List<ZonePrice> zonePrices) {
    return Column(
        children: zonePrices.map((ZonePrice zonePrice) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        margin: const EdgeInsets.symmetric(vertical: 5),
        color: Theme
            .of(context)
            .primaryColor,
        child: ListTile(
          title: Text(zonePrice.zone.city.name + ' / ' + zonePrice.zone.name),
          trailing: Text(
            zonePrice.price.toString() + ' دينار ' ?? ' ',
            style: Theme
                .of(context)
                .textTheme
                .headline4,
          ),
        ),
      );
    }).toList());
  }
}
