import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/helpers/helper.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/reviews_controller.dart';
import '../elements/EmptyReviewsWidget.dart';
import '../elements/ReviewsListWidget.dart';
import '../models/route_argument.dart';

class ReviewDetailsWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  ReviewDetailsWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _ReviewDetailsWidgetState createState() => _ReviewDetailsWidgetState();
}

class _ReviewDetailsWidgetState extends StateMVC<ReviewDetailsWidget> {
  var reviewId;
  ReviewsController _con;

  _ReviewDetailsWidgetState() : super(ReviewsController()) {
    _con = controller;
  }

  @override
  void initState() {
    // route param contains the payment method
    reviewId = widget.routeArgument.id;
    _con.listenForReviewDetails(reviewId);
    _con.listenForReviewHistory(reviewId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).hintColor,
          ),
          centerTitle: true,
          title: Text(
            S.of(context).reviews,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _con.review == null
                  ? SizedBox()
                  : Column(
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(_con.review.averageRating, style: Theme.of(context).textTheme.caption.merge(TextStyle(color: getRatingColor(), fontWeight: FontWeight.bold, fontSize: 24))),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Icon(
                                    LadeedIcons.star__1_,
                                    color: getRatingColor(),
                                    size: 20,
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(_con.review.review),
                              ),
                              SizedBox(height: 10),

                              // ListView.separated(
                              //   padding: EdgeInsets.all(0),
                              //   itemBuilder: (context, index) {
                              //     return ReviewItemWidget(review: reviewsList.elementAt(index));
                              //   },
                              //   separatorBuilder: (context, index) {
                              //     return SizedBox(height: 20);
                              //   },
                              //   itemCount: reviewsList.length,
                              //   primary: false,
                              //   shrinkWrap: true,
                              // ),

                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                                child: Column(
                                  children: [
                                    ListView.separated(
                                      padding: EdgeInsets.all(0),
                                      itemBuilder: (context, aspectIndex) {
                                        return Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              _con.review.aspects.elementAt(aspectIndex).name,
                                              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                            ),
                                            Row(
                                              children: [
                                                Row(
                                                  children: Helper.getStarsListNoCount(double.parse(_con.review.aspects.elementAt(aspectIndex).rate)),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  _con.review.aspects.elementAt(aspectIndex).rate,
                                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                                )
                                              ],
                                            )
                                          ],
                                        );
                                      },
                                      separatorBuilder: (context, index) {
                                        return SizedBox(height: 15);
                                      },
                                      itemCount: _con.review.aspects.length,
                                      primary: false,
                                      shrinkWrap: true,
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                child: ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  title: Text(
                    S.of(context).review_history,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ),
              Divider(
                indent: 16,
                endIndent: 16,
                color: Colors.grey,
              ),
              _con.reviewHistory.isEmpty
                  ? EmptyReviewsWidget()
                  : Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: ReviewsListWidget(reviewsList: _con.reviewHistory),
                    ),
            ],
          ),
        ));
  }

  String getRatingText(context) {
    double rate = double.parse(_con.review.averageRating);
    if (rate == 5.0)
      return S.of(context).ladeed;
    else if (rate >= 4.5 && rate <= 4.9)
      return S.of(context).excellent;
    else if (rate >= 4.0 && rate < 4.5)
      return S.of(context).very_good;
    else if (rate >= 3.5 && rate < 4.0)
      return S.of(context).good;
    else if (rate >= 2.5 && rate < 3.5)
      return S.of(context).ok;
    else if (rate >= 2.0 && rate < 2.5)
      return S.of(context).poor;
    else
      return "";
  }

  Color getRatingColor() {
    double rate = double.parse(_con.review.averageRating);
    if (rate == 5.0)
      return Colors.green;
    else if (rate >= 4.5 && rate <= 4.9)
      return Colors.green[400];
    else if (rate >= 4.0 && rate < 4.5)
      return Colors.lightGreen[400];
    else if (rate >= 3.5 && rate < 4.0)
      return Colors.yellow[700];
    else if (rate >= 2.5 && rate < 3.5)
      return Colors.orange;
    else if (rate >= 2.0 && rate < 2.5)
      return Colors.red;
    else
      return Colors.grey;
  }
}
