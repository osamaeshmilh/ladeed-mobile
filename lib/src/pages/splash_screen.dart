import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as h;
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

import '../controllers/splash_screen_controller.dart';
import '../repository/settings_repository.dart' as settingsRepo;

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends StateMVC<SplashScreen> {
  SplashScreenController _con;

  static const APP_STORE_URL = 'https://apps.apple.com/us/app/id1518440966';
  static const PLAY_STORE_URL = 'https://play.google.com/store/apps/details?id=ly.tanfeed.ladeed';

  SplashScreenState() : super(SplashScreenController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() {
    _con.progress.addListener(() {
      double progress = 0;
      _con.progress.value.values.forEach((_progress) {
        progress += _progress;
      });
      if (progress == 100) {
        try {
          settingsRepo.isFirstLaunch().then((val) {
            if (val) {
              Navigator.of(context).pushNamed('/Intro');
            } else {
              Navigator.of(context).pushReplacementNamed('/Pages', arguments: 2).whenComplete(() {
                PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {
                  try {
                    var response = await h.get("http://ladeed.ly/update.html");
                    if (int.parse(packageInfo.buildNumber) < int.parse(response.body)) {
                      _showVersionDialog(context);
                    }
                    print(response.body);
                  } catch (e) {
                    print(e);
                  }
                  print(packageInfo.buildNumber);
                });
              });
            }
          });
        } catch (e) {}
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.deepOrange,
      body: Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/img/ladeed-white.png',
                width: 180,
                fit: BoxFit.cover,
              ),
              // CircularProgressIndicator(
              //   valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).hintColor),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  _showVersionDialog(context) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "تحديث للتطبيق";
        String message = "يوجد تحديث جديد للتطبيق متاح عبر المتجر الان, قم بتحميله!";
        String btnLabel = "تحميل";
        String btnLabelCancel = "لاحقا";
        return Platform.isIOS
            ? new CupertinoAlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => launch(APP_STORE_URL),
                  ),
                  FlatButton(
                    child: Text(btnLabelCancel),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              )
            : new AlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => launch(PLAY_STORE_URL),
                  ),
                  FlatButton(
                    child: Text(btnLabelCancel),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              );
      },
    );
  }
}
