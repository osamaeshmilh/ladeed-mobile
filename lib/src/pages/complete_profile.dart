import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';
import '../controllers/settings_controller.dart';
import '../elements/BlockButtonWidget.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as repository;

class CompleteProfileWidget extends StatefulWidget {
  const CompleteProfileWidget({Key key}) : super(key: key);

  @override
  _CompleteProfileWidgetState createState() => _CompleteProfileWidgetState();
}

class _CompleteProfileWidgetState extends StateMVC<CompleteProfileWidget> {
  SettingsController _con;
  GlobalKey<FormState> _profileSettingsFormKey = new GlobalKey<FormState>();
  final phoneController = TextEditingController();
  final codeController = TextEditingController();
  String verificationId;
  auth.FirebaseAuth _auth = auth.FirebaseAuth.instance;
  PageController pageController = PageController(initialPage: 0);

  String smsCode;
  String phone;
  User user;

  _CompleteProfileWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).complete_profile,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: pageController,
          children: <Widget>[phoneForm(), codeForm()],
        ),
      ),
    );
  }

  String replaceCharAt(String oldString, int index, String newChar) {
    return oldString.substring(0, index) + newChar + oldString.substring(index + 1);
  }

  Future<bool> loginUser(String phone) async {
    _auth.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: Duration(seconds: 120),
        verificationCompleted: (auth.AuthCredential credential) async {
          auth.UserCredential result = await _auth.signInWithCredential(credential);
          auth.User user = result.user;
          if (user != null) {
            _submit();
          } else {
            print("Error");
          }
          _con.hideLoader();

          //This callback would gets called when verification is done auto maticlly
        },
        verificationFailed: (auth.FirebaseAuthException exception) {
          Fluttertoast.showToast(msg: 'حدث خطأ في عملية التحقق !');
          _con.hideLoader();
        },
        codeSent: (String verificationId, [int forceResendingToken]) async {
          this.verificationId = verificationId;
          print('code sent ' + verificationId);
          pageController.animateToPage(1, duration: kTabScrollDuration, curve: Curves.ease);
          _con.hideLoader();
        },
        codeAutoRetrievalTimeout: null);
  }

  Future verifyCode(verificationId, smsCode) async {
    print('code ' + smsCode);
    try {
      auth.AuthCredential credential = auth.PhoneAuthProvider.credential(verificationId: verificationId, smsCode: smsCode);
      auth.UserCredential result = await _auth.signInWithCredential(credential);
      auth.User user = result.user;
      if (user != null) {
        if (this.user != null) {
          _submit();
          Navigator.pop(context);
        }
      } else {
        SnackBar(
          content: Text('رمز تفعيل خاطئ!'),
        );
        print("Error");
      }
    } catch (e) {
      Alert(context: context, title: S.of(context).sms_code, desc: 'رمز تفعيل خاطئ!', type: AlertType.error).show();
      Fluttertoast.showToast(msg: 'رمز تفعيل خاطئ!');
      print('er ' + e.toString());
    }
  }

  void _submit() {
    this.user.phone = phone;
    this.user.mobile = phone;
    this.user.bio = verificationId;
    _con.update(this.user);
    Navigator.pop(context);
  }

  Widget phoneForm() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Form(
          key: _profileSettingsFormKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 50.0),
              TextFormField(
                controller: phoneController,
                keyboardType: TextInputType.number,
                onSaved: (input) => phone = replaceCharAt(input, 0, '+218'),
                validator: MultiValidator([
                  PatternValidator(
                    r'^(0?(9[0-9]{8}))$',
                    errorText: S.of(context).not_a_valid_phone,
                  )
                ]),
                decoration: InputDecoration(
                  labelText: S.of(context).phone,
                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                  contentPadding: EdgeInsets.all(12),
                  hintText: '0912345678',
                  hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                  prefixIcon: Icon(LadeedIcons.telephone, color: Theme.of(context).accentColor),
                  border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                ),
                onChanged: (c) {
                  if (_profileSettingsFormKey.currentState.validate()) {
                    _profileSettingsFormKey.currentState.save();
                  }
                },
              ),
              SizedBox(height: 30.0),
              BlockButtonWidget(
                text: Text(
                  "ارسال رمز التفعيل",
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                color: Theme.of(context).accentColor,
                onPressed: () {
                  _con.showLoader();
                  repository.getCurrentUser().then((user) {
                    this.user = user;
                    loginUser(replaceCharAt(phoneController.text, 0, '+218'));
                  });
                },
              ),
              SizedBox(height: 30.0),
              Center(child: Text('في حالة عدم وصول رمز التفعيل تواصل معنا لتفعيل حسابك :', textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1)),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.messenger,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("https://m.me/ladeed.ly/");
                        }),
                    IconButton(
                        icon: new Icon(
                          FontAwesomeIcons.whatsapp,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("whatsapp://send?phone=+218913652299");
                        }),
                    IconButton(
                        icon: new Icon(
                          LadeedIcons.telephone,
                          size: 25.0,
                          color: Colors.blueGrey,
                        ),
                        onPressed: () {
                          launch("tel:+218913652299");
                        }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget codeForm() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: <Widget>[
            SizedBox(height: 50.0),
            Directionality(
              textDirection: TextDirection.ltr,
              child: PinCodeTextField(
                length: 6,
                obscureText: true,
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  borderRadius: BorderRadius.circular(5),
                  fieldHeight: 50,
                  fieldWidth: 40,
                  activeFillColor: Colors.white,
                ),
                animationDuration: Duration(milliseconds: 300),
                backgroundColor: Colors.transparent,
                enableActiveFill: false,
                controller: codeController,
                onCompleted: (code) {
                  print("Completed " + code);
                  verifyCode(verificationId, code);
                },
                onChanged: (value) {
                  print(value);
                  setState(() {
                    smsCode = value;
                  });
                },
                beforeTextPaste: (text) {
                  smsCode = text;
                  return true;
                },
                appContext: context,
              ),
            ),
            SizedBox(height: 30.0),
            BlockButtonWidget(
              text: Text(
                "تأكيد",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                verifyCode(verificationId, codeController.text);
              },
            ),
          ],
        ),
      ),
    );
  }
}
