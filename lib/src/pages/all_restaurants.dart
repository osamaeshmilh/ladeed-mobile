import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/controllers/restaurant_controller.dart';
import 'package:food_delivery_app/src/elements/ShoppingCartButtonWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../elements/CardWidget.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/route_argument.dart';

class AllRestaurantsWidget extends StatefulWidget {
  final String heroTag;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  AllRestaurantsWidget({Key key, this.heroTag, this.parentScaffoldKey}) : super(key: key);

  @override
  _AllRestaurantsWidgetState createState() => _AllRestaurantsWidgetState();
}

class _AllRestaurantsWidgetState extends StateMVC<AllRestaurantsWidget> {
  RestaurantController _con;

  _AllRestaurantsWidgetState() : super(RestaurantController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForRestaurants();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          S.of(context).restaurants,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _con.restaurants.isEmpty
                ? CircularLoadingWidget(height: 100)
                : Expanded(
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (scrollNotification) {
                        if (scrollNotification.metrics.pixels == scrollNotification.metrics.maxScrollExtent) {
                          _con.loadMoreRestaurants();
                        }
                        return true;
                      },
                      child: ListView(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                            child: ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.symmetric(vertical: 0),
                              title: Text(
                                S.of(context).restaurants_results,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                              trailing: Text(
                                _con.restaurants.length.toString() + ' ' + S.of(context).result,
                              ),
                            ),
                          ),
                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: _con.restaurants.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/Details',
                                      arguments: RouteArgument(
                                        id: '0',
                                        param: _con.restaurants.elementAt(index).id,
                                        heroTag: _con.restaurants.elementAt(index).id,
                                      ));
                                },
                                child: CardWidget(restaurant: _con.restaurants.elementAt(index), heroTag: _con.restaurants.elementAt(index).id),
                              );
                            },
                          ),
                          _con.isLoading ? CircularLoadingWidget(height: 100) : SizedBox()
                        ],
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
