import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/cart_controller.dart';
import '../elements/CartBottomDetailsWidget.dart';
import '../elements/CartItemWidget.dart';
import '../elements/EmptyCartWidget.dart';
import '../models/route_argument.dart';

class CartWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  CartWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _CartWidgetState createState() => _CartWidgetState();
}

class _CartWidgetState extends StateMVC<CartWidget> {
  CartController _con;

  var couponText;

  _CartWidgetState() : super(CartController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForCarts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      bottomNavigationBar: CartBottomDetailsWidget(con: _con),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            if (widget.routeArgument != null) {
              Navigator.of(context).pushReplacementNamed(widget.routeArgument.param, arguments: RouteArgument(id: widget.routeArgument.id));
            } else {
              Navigator.of(context).pushReplacementNamed('/Pages', arguments: 2);
            }
          },
          icon: Icon(Icons.arrow_back),
          color: Theme.of(context).hintColor,
        ),
        centerTitle: true,
        title: Text(
          S.of(context).cart,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshCarts,
        child: _con.carts.isEmpty
            ? EmptyCartWidget()
            : Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 10),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(vertical: 0),
                          leading: Icon(
                            Icons.shopping_cart,
                            color: Theme.of(context).hintColor,
                          ),
                          title: Text(
                            S.of(context).shopping_cart,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.headline4,
                          ),
                          // subtitle: Text(
                          //   S.of(context).verify_your_quantity_and_click_checkout,
                          //   maxLines: 1,
                          //   overflow: TextOverflow.ellipsis,
                          //   style: Theme.of(context).textTheme.caption,
                          // ),
                          trailing: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                            ),
                            child: Text(
                              "لديك قسيمة ؟",
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                            ),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  // return object of type Dialog
                                  return AlertDialog(
                                    title: new Text("رقم القسيمة"),
                                    content: TextField(
                                      controller: couponText,
                                      decoration: InputDecoration(
                                        hintText: 'ادخل رقم القسيمة ',
                                      ),
                                    ),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      Row(
                                        children: <Widget>[
                                          new FlatButton(
                                            child: new Text("الغاء"),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          new FlatButton(
                                            onPressed: () {
                                              // order.active = false;
                                              // order.hint = hintText.text;
                                              // listenCancelOrder().then((value) {
                                              //   Navigator.of(context).pop();
                                              // });
                                            },
                                            child: new Text("حفظ"),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            textColor: Theme.of(context).accentColor.withOpacity(1),
                          ),
                        ),
                      ),
                      ListView.separated(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        primary: true,
                        itemCount: _con.carts.length,
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 15);
                        },
                        itemBuilder: (context, index) {
                          return CartItemWidget(
                            cart: _con.carts.elementAt(index),
                            heroTag: 'cart',
                            increment: () {
                              _con.incrementQuantity(_con.carts.elementAt(index));
                            },
                            decrement: () {
                              _con.decrementQuantity(_con.carts.elementAt(index));
                            },
                            onDismissed: () {
                              _con.removeFromCart(_con.carts.elementAt(index));
                            },
                          );
                        },
                      ),
                    ],
                  ),
                  // Container(
                  //   padding: const EdgeInsets.all(18),
                  //   margin: EdgeInsets.only(bottom: 15),
                  //   decoration: BoxDecoration(
                  //       color: Theme.of(context).primaryColor,
                  //       borderRadius: BorderRadius.all(Radius.circular(20)),
                  //       boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, 2), blurRadius: 5.0)]),
                  //   child: TextField(
                  //     keyboardType: TextInputType.text,
                  //     onSubmitted: (String value) {
                  //       _con.doApplyCoupon(value);
                  //     },
                  //     cursorColor: Theme.of(context).accentColor,
                  //     controller: TextEditingController()..text = coupon.value?.code ?? '',
                  //     decoration: InputDecoration(
                  //       contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                  //       floatingLabelBehavior: FloatingLabelBehavior.always,
                  //       hintStyle: Theme.of(context).textTheme.bodyText1,
                  //       suffixText: coupon.value?.valid == null ? '' : (coupon.value.valid ? S.of(context).validCouponCode : S.of(context).invalidCouponCode),
                  //       suffixStyle: Theme.of(context).textTheme.caption.merge(TextStyle(color: _con.getCouponIconColor())),
                  //       suffixIcon: Padding(
                  //         padding: const EdgeInsets.symmetric(horizontal: 15),
                  //         child: Icon(
                  //           Icons.confirmation_number,
                  //           color: _con.getCouponIconColor(),
                  //           size: 28,
                  //         ),
                  //       ),
                  //       hintText: S.of(context).haveCouponCode,
                  //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(30), borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                  //       focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(30), borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                  //       enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(30), borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
      ),
    );
  }
}
