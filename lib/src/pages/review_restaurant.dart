import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:food_delivery_app/src/controllers/reviews_controller.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/route_argument.dart';

class RestaurantReviewWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  RestaurantReviewWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _RestaurantReviewWidgetState createState() {
    return _RestaurantReviewWidgetState();
  }
}

class _RestaurantReviewWidgetState extends StateMVC<RestaurantReviewWidget> {
  ReviewsController _con;

  _RestaurantReviewWidgetState() : super(ReviewsController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForRestaurant(restaurantId: widget.routeArgument.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        body: RefreshIndicator(
            onRefresh: _con.refreshRestaurant,
            child: _con.restaurant == null
                ? CircularLoadingWidget(height: 500)
                : SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Stack(
                              alignment: AlignmentDirectional.bottomCenter,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 30),
                                  child: SizedBox(
                                    height: 150,
                                    width: double.infinity,
                                    child: Hero(
                                      tag: widget.routeArgument.heroTag + _con.restaurant.id,
                                      child: CachedNetworkImage(
                                        fit: BoxFit.cover,
                                        imageUrl: _con.restaurant.image.url,
                                        placeholder: (context, url) => Image.asset(
                                          'assets/img/loading.gif',
                                          fit: BoxFit.cover,
                                        ),
                                        errorWidget: (context, url, error) => Icon(Icons.error),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 60,
                                  width: 110,
                                  child: Chip(
                                    padding: EdgeInsets.all(10),
                                    label: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(_con.restaurant.rate, style: Theme.of(context).textTheme.headline3.merge(TextStyle(color: Theme.of(context).primaryColor))),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Icon(
                                          LadeedIcons.star__1_,
                                          color: Theme.of(context).primaryColor,
                                          size: 25,
                                        ),
                                      ],
                                    ),
                                    backgroundColor: Theme.of(context).accentColor.withOpacity(0.9),
                                    shape: StadiumBorder(),
                                  ),
                                ),
                              ],
                            ),
                            Positioned(
                              top: 30,
                              right: 15,
                              child: IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.arrow_back,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Text(
                          _con.restaurant.name,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline3,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, -2), blurRadius: 5.0)]),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Text(S.of(context).how_would_you_rate_this_restaurant_, textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1),
                              SizedBox(height: 20),
                              ListView.separated(
                                padding: EdgeInsets.all(0),
                                itemBuilder: (context, aspectIndex) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          _con.restaurantReview.aspects.elementAt(aspectIndex).name,
                                          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: List.generate(5, (index) {
                                            return InkWell(
                                              onTap: () {
                                                setState(() {
                                                  _con.restaurantReview.aspects.elementAt(aspectIndex).rate = (index + 1).toString();
                                                });
                                              },
                                              child: index < int.parse(_con.restaurantReview.aspects.elementAt(aspectIndex).rate)
                                                  ? Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                                      child: Icon(LadeedIcons.star__1_, size: 25, color: Color(0xFFFFB24D)),
                                                    )
                                                  : Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                                      child: Icon(LadeedIcons.star2, size: 25, color: Color(0xFFFFB24D)),
                                                    ),
                                            );
                                          }),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                separatorBuilder: (context, index) {
                                  return SizedBox(height: 15);
                                },
                                itemCount: _con.restaurantReview.aspects.length,
                                primary: false,
                                shrinkWrap: true,
                              ),
                              SizedBox(height: 10),
                              TextField(
                                onChanged: (text) {
                                  _con.restaurantReview.review = text;
                                },
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: S.of(context).tell_us_about_this_restaurant,
                                  hintStyle: Theme.of(context).textTheme.caption.merge(TextStyle(fontSize: 14)),
                                  border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.1))),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.1))),
                                ),
                              ),
                              SizedBox(height: 10),
                              FlatButton.icon(
                                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 18),
                                onPressed: () {
                                  if (_con.restaurantReview.review == null || _con.restaurantReview.review == '' || _con.restaurantReview.isAspectsRated()) {
                                    _con.scaffoldKey?.currentState?.showSnackBar(SnackBar(
                                      content: Text("الرجاء كتابة تقييم وتحديد عدد النجوم!"),
                                    ));
                                  } else {
                                    _con.addRestaurantReviewNoOrder(_con.restaurantReview);
                                    FocusScope.of(context).unfocus();
                                  }
                                },
                                shape: StadiumBorder(),
                                label: Text(
                                  S.of(context).submit,
                                  style: TextStyle(color: Theme.of(context).primaryColor),
                                ),
                                icon: Icon(
                                  Icons.check,
                                  color: Theme.of(context).primaryColor,
                                ),
                                textColor: Theme.of(context).primaryColor,
                                color: Theme.of(context).accentColor,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  )));
  }
}
