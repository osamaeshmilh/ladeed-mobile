import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/settings_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../repository/user_repository.dart';

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends StateMVC<SettingsWidget> {
  SettingsController _con;
  TextEditingController passwordController = new TextEditingController();

  _SettingsWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            S.of(context).settings,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: currentUser.value.id == null
            ? CircularLoadingWidget(height: 500)
            : SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 7),
                child: Column(
                  children: <Widget>[
//                    Padding(
//                      padding: const EdgeInsets.symmetric(horizontal: 20),
//                      child: SearchBarWidget(),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//                      child: Row(
//                        children: <Widget>[
//                          Expanded(
//                            child: Column(
//                              children: <Widget>[
//                                Text(
//                                  currentUser.value.name,
//                                  textAlign: TextAlign.left,
//                                  style: Theme.of(context).textTheme.headline3,
//                                ),
//                                Text(
//                                  currentUser.value.email,
//                                  style: Theme.of(context).textTheme.caption,
//                                )
//                              ],
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                            ),
//                          ),
//                          SizedBox(
//                              width: 55,
//                              height: 55,
//                              child: InkWell(
//                                borderRadius: BorderRadius.circular(300),
//                                onTap: () {
//                                  Navigator.of(context).pushNamed('/Profile');
//                                },
//                                child: CircleAvatar(
//                                  backgroundImage: NetworkImage(currentUser.value.image.thumb),
//                                ),
//                              )),
//                        ],
//                      ),
//                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.1), offset: Offset(0, 1), blurRadius: 1)],
                      ),
                      child: ListView(
                        shrinkWrap: true,
                        primary: false,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(LadeedIcons.user1),
                            title: Text(
                              S.of(context).profile_settings,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            trailing: FlatButton(
                              child: Text(
                                S.of(context).edit,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              padding: EdgeInsets.all(0),
                              onPressed: () {
                                Navigator.of(context).pushNamed('/CompleteProfile');
                              },
                            ),
                          ),
                          ListTile(
                            onTap: () {},
                            dense: true,
                            title: Text(
                              S.of(context).full_name,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                            trailing: Text(
                              currentUser.value.name,
                              style: TextStyle(color: Theme.of(context).focusColor),
                            ),
                          ),
                          ListTile(
                            onTap: () {},
                            dense: true,
                            title: Text(
                              S.of(context).email,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                            trailing: Text(
                              currentUser.value.email,
                              style: TextStyle(color: Theme.of(context).focusColor),
                            ),
                          ),
                          ListTile(
                            onTap: () {},
                            dense: true,
                            title: Text(
                              S.of(context).phone,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                            trailing: Text(
                              currentUser.value.phone ?? ' ',
                              style: TextStyle(color: Theme.of(context).focusColor),
                            ),
                          ),
//                          ListTile(
//                            onTap: () {},
//                            dense: true,
//                            title: Text(
//                              S.of(context).address,
//                              style: Theme.of(context).textTheme.bodyText2,
//                            ),
//                            trailing: Text(
//                              Helper.limitString(currentUser.value.address ?? S.of(context).unknown),
//                              overflow: TextOverflow.fade,
//                              softWrap: false,
//                              style: TextStyle(color: Theme.of(context).focusColor),
//                            ),
//                          ),
//                          ListTile(
//                            onTap: () {},
//                            dense: true,
//                            title: Text(
//                              S.of(context).about,
//                              style: Theme.of(context).textTheme.bodyText2,
//                            ),
//                            trailing: Text(
//                              Helper.limitString(currentUser.value.bio),
//                              overflow: TextOverflow.fade,
//                              softWrap: false,
//                              style: TextStyle(color: Theme.of(context).focusColor),
//                            ),
//                          ),
                        ],
                      ),
                    ),
//                    Container(
//                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
//                      decoration: BoxDecoration(
//                        color: Theme.of(context).primaryColor,
//                        borderRadius: BorderRadius.circular(6),
//                        boxShadow: [BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.15), offset: Offset(0, 3), blurRadius: 10)],
//                      ),
//                      child: ListView(
//                        shrinkWrap: true,
//                        primary: false,
//                        children: <Widget>[
//                          ListTile(
//                            leading: Icon(Icons.credit_card),
//                            title: Text(
//                              S.of(context).payments_settings,
//                              style: Theme.of(context).textTheme.bodyText1,
//                            ),
//                            trailing: ButtonTheme(
//                              padding: EdgeInsets.all(0),
//                              minWidth: 50.0,
//                              height: 25.0,
//                              child: PaymentSettingsDialog(
//                                creditCard: _con.creditCard,
//                                onChanged: () {
//                                  _con.updateCreditCard(_con.creditCard);
//                                  //setState(() {});
//                                },
//                              ),
//                            ),
//                          ),
//                          ListTile(
//                            dense: true,
//                            title: Text(
//                              S.of(context).default_credit_card,
//                              style: Theme.of(context).textTheme.bodyText2,
//                            ),
//                            trailing: Text(
//                              _con.creditCard.number.isNotEmpty ? _con.creditCard.number.replaceRange(0, _con.creditCard.number.length - 4, '...') : '',
//                              style: TextStyle(color: Theme.of(context).focusColor),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.1), offset: Offset(0, 1), blurRadius: 1)],
                      ),
                      child: ListView(
                        shrinkWrap: true,
                        primary: false,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(LadeedIcons.settings),
                            title: Text(
                              S.of(context).app_settings,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  // return object of type Dialog
                                  return AlertDialog(
                                    title: new Text("ادخل كلمة المرور الجديدة"),
                                    content: TextField(
                                      obscureText: true,
                                      controller: passwordController,
                                      decoration: InputDecoration(
                                        hintText: 'ادخل كلمة المرور الجديدة ',
                                      ),
                                    ),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      Row(
                                        children: <Widget>[
                                          new FlatButton(
                                            child: new Text("الغاء"),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          new FlatButton(
                                            onPressed: () {
                                              currentUser.value.password = passwordController.text;
                                              _con.updatePass(currentUser.value);
                                              Navigator.pop(context);
                                            },
                                            child: new Text("حفظ"),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            dense: true,
                            title: Row(
                              children: <Widget>[
                                Icon(
                                  LadeedIcons.padlock,
                                  size: 22,
                                  color: Theme.of(context).focusColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  S.of(context).change_password,
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                              ],
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('/Languages');
                            },
                            dense: true,
                            title: Row(
                              children: <Widget>[
                                Icon(
                                  LadeedIcons.translate,
                                  size: 22,
                                  color: Theme
                                      .of(context)
                                      .focusColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  S.of(context).languages,
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                              ],
                            ),
                            trailing: Text(
                              S.of(context).english,
                              style: TextStyle(color: Theme.of(context).focusColor),
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('/DeliveryAddresses');
                            },
                            dense: true,
                            title: Row(
                              children: <Widget>[
                                Icon(
                                  LadeedIcons.location_pin,
                                  size: 22,
                                  color: Theme
                                      .of(context)
                                      .focusColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  S.of(context).delivery_addresses,
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                              ],
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('/Help');
                            },
                            dense: true,
                            title: Row(
                              children: <Widget>[
                                Icon(
                                  LadeedIcons.about,
                                  size: 22,
                                  color: Theme
                                      .of(context)
                                      .focusColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  S
                                      .of(context)
                                      .help_support,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .bodyText2,
                                ),
                              ],
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('/About');
                            },
                            dense: true,
                            title: Row(
                              children: <Widget>[
                                Icon(
                                  LadeedIcons.information,
                                  size: 22,
                                  color: Theme
                                      .of(context)
                                      .focusColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  S
                                      .of(context)
                                      .about_us,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .bodyText2,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ));
  }
}
