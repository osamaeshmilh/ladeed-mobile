import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/food_controller.dart';
import '../elements/AddToCartAlertDialog.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/ShoppingCartFloatButtonWidget.dart';
import '../helpers/helper.dart';
import '../models/route_argument.dart';
import '../repository/user_repository.dart';

// ignore: must_be_immutable
class FoodWidget extends StatefulWidget {
  RouteArgument routeArgument;

  FoodWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _FoodWidgetState createState() {
    return _FoodWidgetState();
  }
}

class _FoodWidgetState extends StateMVC<FoodWidget> {
  FoodController _con;

  var selectedSizeId;

  _FoodWidgetState() : super(FoodController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForFood(foodId: widget.routeArgument.id);
    _con.listenForCart();
    _con.listenForFavorite(foodId: widget.routeArgument.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      body: _con.food == null || _con.food?.image == null
          ? CircularLoadingWidget(height: 400)
          : RefreshIndicator(
              onRefresh: _con.refreshFood,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 125),
                    padding: EdgeInsets.only(bottom: 15),
                    child: CustomScrollView(
                      primary: true,
                      shrinkWrap: false,
                      slivers: <Widget>[
                        SliverAppBar(
                          backgroundColor: Theme.of(context).accentColor.withOpacity(0.9),
                          expandedHeight: 200,
                          elevation: 0,
                          iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
                          flexibleSpace: FlexibleSpaceBar(
                            collapseMode: CollapseMode.parallax,
                            background: Hero(
                              tag: widget.routeArgument.heroTag ?? '' + _con.food.id,
                              child: CachedNetworkImage(
                                fit: BoxFit.cover,
                                imageUrl: _con.food.image.url,
                                placeholder: (context, url) => Image.asset(
                                  'assets/img/loading.gif',
                                  fit: BoxFit.cover,
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                              ),
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                            child: Wrap(
                              runSpacing: 4,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            _con.food?.name ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: Theme.of(context).textTheme.headline3,
                                          ),
                                          Text(
                                            _con.food?.restaurant?.name ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: Theme.of(context).textTheme.bodyText2,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Helper.getPrice(
                                            _con.food.price,
                                            context,
                                            style: Theme.of(context).textTheme.headline2,
                                          ),
                                          _con.food.discountPrice > 0
                                              ? Helper.getPrice(_con.food.discountPrice, context, style: Theme.of(context).textTheme.bodyText2.merge(TextStyle(decoration: TextDecoration.lineThrough)))
                                              : SizedBox(height: 0),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(child: SizedBox(height: 0)),
                                  ],
                                ),
                                Helper.applyHtml(context, _con.food.description, style: TextStyle(fontSize: 12)),
                                _con.food.extraGroups == null
                                    ? CircularLoadingWidget(height: 100)
                                    : ListView.separated(
                                        padding: EdgeInsets.all(0),
                                        itemBuilder: (context, extraGroupIndex) {
                                          var extraGroup = _con.food.extraGroups.elementAt(extraGroupIndex);
                                          return !extraGroup.isMultiSelect
                                              ? Wrap(
                                                  children: <Widget>[
                                                    ExpansionTile(
                                                      leading: Icon(
                                                        Icons.settings,
                                                        color: Theme.of(context).hintColor,
                                                      ),
                                                      title: Text(
                                                        extraGroup.name,
                                                        style: Theme.of(context).textTheme.subtitle1,
                                                      ),
                                                      initiallyExpanded: true,
                                                      children: [
                                                        ListView.separated(
                                                          padding: EdgeInsets.all(0),
                                                          itemBuilder: (context, extraIndex) {
                                                            return extraGroup.id == _con.food.extras.elementAt(extraIndex).extraGroupId
                                                                ? RadioListTile(
                                                                    title: Text(_con.food.extras.elementAt(extraIndex).name),
                                                                    subtitle: Helper.getPrice(
                                                                      _con.food.extras.elementAt(extraIndex).price,
                                                                      context,
                                                                      style: Theme.of(context).textTheme.caption,
                                                                    ),
                                                                    value: _con.food.extras.elementAt(extraIndex).id,
                                                                    onChanged: (val) {
                                                                      setState(() {
                                                                        _con.selectedSizeId = val;
                                                                      });
                                                                      _con.selectSize();
                                                                    },
                                                                    groupValue: _con.selectedSizeId,
                                                                  )
                                                                : SizedBox();
                                                          },
                                                          separatorBuilder: (context, index) {
                                                            return SizedBox(height: 0);
                                                          },
                                                          itemCount: _con.food.extras.length,
                                                          primary: false,
                                                          shrinkWrap: true,
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                )
                                              : Wrap(
                                                  children: <Widget>[
                                                    ExpansionTile(
                                                      leading: Icon(
                                                        Icons.add_circle_outline,
                                                        color: Theme.of(context).hintColor,
                                                      ),
                                                      title: Text(
                                                        extraGroup.name,
                                                        style: Theme.of(context).textTheme.subtitle1,
                                                      ),
                                                      children: [
                                                        ListView.separated(
                                                          padding: EdgeInsets.all(0),
                                                          itemBuilder: (context, extraIndex) {
                                                            return extraGroup.id == _con.food.extras.elementAt(extraIndex).extraGroupId
                                                                ? CheckboxListTile(
                                                                    title: Text(_con.food.extras.elementAt(extraIndex).name),
                                                                    subtitle: Helper.getPrice(
                                                                      _con.food.extras.elementAt(extraIndex).price,
                                                                      context,
                                                                      style: Theme.of(context).textTheme.caption,
                                                                    ),
                                                                    value: _con.food.extras.elementAt(extraIndex).checked,
                                                                    onChanged: (val) {
                                                                      setState(() {
                                                                        _con.food.extras.elementAt(extraIndex).checked = val;
                                                                      });

                                                                      _con.calculateTotal();
                                                                    })
                                                                : SizedBox();
                                                          },
                                                          separatorBuilder: (context, index) {
                                                            return SizedBox(height: 0);
                                                          },
                                                          itemCount: _con.food.extras.length,
                                                          primary: false,
                                                          shrinkWrap: true,
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                );
                                        },
                                        separatorBuilder: (context, index) {
                                          return SizedBox(height: 20);
                                        },
                                        itemCount: _con.food.extraGroups.length,
                                        primary: false,
                                        shrinkWrap: true,
                                      ),
                                ListTile(
                                  dense: true,
                                  leading: Icon(
                                    Icons.donut_small,
                                    color: Theme.of(context).hintColor,
                                  ),
                                  title: Text(
                                    S.of(context).ingredients,
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                ),
                                Helper.applyHtml(context, _con.food.ingredients, style: TextStyle(fontSize: 12)),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  onChanged: (text) {
                                    // _con.restaurantReview.review = text;
                                  },
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    contentPadding: EdgeInsets.all(12),
                                    hintText: S.of(context).food_notes,
                                    hintStyle: Theme.of(context).textTheme.caption.merge(TextStyle(fontSize: 14)),
                                    border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.1))),
                                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.1))),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 32,
                    left: 20,
                    child: _con.loadCart
                        ? SizedBox(
                            width: 60,
                            height: 60,
                            child: RefreshProgressIndicator(),
                          )
                        : ShoppingCartFloatButtonWidget(
                            iconColor: Theme.of(context).primaryColor,
                            labelColor: Theme.of(context).hintColor,
                            routeArgument: RouteArgument(param: '/Food', id: _con.food.id),
                          ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      height: 150,
                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                          boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, -2), blurRadius: 5.0)]),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    S.of(context).quantity,
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    IconButton(
                                      onPressed: () {
                                        _con.decrementQuantity();
                                      },
                                      iconSize: 30,
                                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                      icon: Icon(
                                        LadeedIcons.minus,
                                        size: 24,
                                      ),
                                      color: Theme
                                          .of(context)
                                          .hintColor,
                                    ),
                                    Text(_con.quantity.toString(), style: Theme.of(context).textTheme.subtitle1),
                                    IconButton(
                                      onPressed: () {
                                        _con.incrementQuantity();
                                      },
                                      iconSize: 30,
                                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                      icon: Icon(
                                        LadeedIcons.more,
                                        size: 24,
                                      ),
                                      color: Theme
                                          .of(context)
                                          .hintColor,
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: _con.favorite?.id != null
                                      ? OutlineButton(
                                          onPressed: () {
                                            _con.removeFromFavorite(_con.favorite);
                                          },
                                          padding: EdgeInsets.symmetric(vertical: 14),
                                          color: Theme.of(context).primaryColor,
                                          shape: StadiumBorder(),
                                          borderSide: BorderSide(color: Theme.of(context).accentColor),
                                          child: Icon(
                                            LadeedIcons.heart_full,
                                            color: Theme
                                                .of(context)
                                                .accentColor,
                                          ))
                                      : FlatButton(
                                          onPressed: () {
                                            if (currentUser.value.apiToken == null) {
                                              Navigator.of(context).pushNamed("/Login");
                                            } else {
                                              _con.addToFavorite(_con.food);
                                            }
                                          },
                                          padding: EdgeInsets.symmetric(vertical: 14),
                                          color: Theme.of(context).accentColor,
                                          shape: StadiumBorder(),
                                          child: Icon(
                                            LadeedIcons.heart_full,
                                            color: Theme
                                                .of(context)
                                                .primaryColor,
                                          )),
                                ),
                                SizedBox(width: 10),
                                Stack(
                                  fit: StackFit.loose,
                                  alignment: AlignmentDirectional.centerEnd,
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width - 110,
                                      child: FlatButton(
                                        onPressed: () {
                                          if (currentUser.value.apiToken == null) {
                                            Navigator.of(context).pushNamed("/Login");
                                          } else {
                                            if (_con.isSameRestaurants(_con.food)) {
                                              _con.addToCart(_con.food);
                                            } else {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  // return object of type Dialog
                                                  return AddToCartAlertDialogWidget(
                                                      oldFood: _con.carts.elementAt(0)?.food,
                                                      newFood: _con.food,
                                                      onPressed: (food, {reset: true}) {
                                                        return _con.addToCart(_con.food, reset: true);
                                                      });
                                                },
                                              );
                                            }
                                          }
                                        },
                                        padding: EdgeInsets.symmetric(vertical: 14),
                                        color: Theme.of(context).accentColor,
                                        shape: StadiumBorder(),
                                        child: Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.symmetric(horizontal: 20),
                                          child: Text(
                                            S.of(context).add_to_cart,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(color: Theme.of(context).primaryColor),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 20),
                                      child: Helper.getPrice(
                                        _con.total,
                                        context,
                                        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
