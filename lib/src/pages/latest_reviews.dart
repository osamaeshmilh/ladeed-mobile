import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/reviews_controller.dart';
import '../elements/EmptyReviewsWidget.dart';
import '../elements/ReviewItemWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';

class LatestReviewsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  LatestReviewsWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _LatestReviewsWidgetState createState() => _LatestReviewsWidgetState();
}

class _LatestReviewsWidgetState extends StateMVC<LatestReviewsWidget> {
  ReviewsController _con;

  _LatestReviewsWidgetState() : super(ReviewsController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForLatestReviews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          S.of(context).latest_reviews,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshLatestReviews,
        child: _con.latestReviews.isEmpty
            ? EmptyReviewsWidget()
            : SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 10),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0),
                        leading: Icon(
                          Icons.rate_review,
                          color: Theme.of(context).hintColor,
                        ),
                        title: Text(
                          S.of(context).latest_reviews,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                    Divider(
                      indent: 16,
                      endIndent: 16,
                      color: Colors.grey,
                    ),
                    ListView.separated(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _con.latestReviews.length,
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 15);
                      },
                      itemBuilder: (context, index) {
                        return ReviewItemWidget(review: _con.latestReviews.elementAt(index));
                      },
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
