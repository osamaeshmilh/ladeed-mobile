import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/elements/CheckoutBottomDetailsWidget.dart';
import 'package:food_delivery_app/src/elements/DeliveryAddressBottomSheetWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/delivery_pickup_controller.dart';
import '../elements/DeliveryAddressesItemWidget.dart';
import '../elements/NotDeliverableAddressesItemWidget.dart';
import '../elements/PickUpMethodItemWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';
import '../helpers/helper.dart';
import '../models/address.dart';
import '../models/payment_method.dart';
import '../models/route_argument.dart';

class DeliveryPickupWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  DeliveryPickupWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _DeliveryPickupWidgetState createState() => _DeliveryPickupWidgetState();
}

class _DeliveryPickupWidgetState extends StateMVC<DeliveryPickupWidget> {
  DeliveryPickupController _con;

  _DeliveryPickupWidgetState() : super(DeliveryPickupController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_con.list == null) {
      _con.list = new PaymentMethodList(context);
//      widget.pickup = widget.list.pickupList.elementAt(0);
//      widget.delivery = widget.list.pickupList.elementAt(1);
    }
    return Scaffold(
      key: _con.scaffoldKey,
      bottomNavigationBar: CheckoutBottomDetailsWidget(con: _con),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).delivery_or_pickup,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 0, bottom: 0, left: 20, right: 10),
                  child: ListTile(
                      contentPadding: EdgeInsets.symmetric(vertical: 0),
                      leading: Icon(
                        Icons.map,
                        color: Theme.of(context).hintColor,
                      ),
                      title: Text(
                        S.of(context).delivery,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      subtitle: _con.carts.isNotEmpty && Helper.canDelivery(_con.carts[0].food.restaurant, carts: _con.carts)
                          ? Text(
                              'توصيل الطلب الى عنوانك',
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.caption,
                            )
                          : Text(
                              S.of(context).deliveryMethodNotAllowed,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.caption,
                            ),
                      trailing: FlatButton(
                        onPressed: () {
                          var bottomSheetController = _con.scaffoldKey?.currentState.showBottomSheet(
                            (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: _con.scaffoldKey),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                            ),
                          );
                          bottomSheetController.closed.then((value) {
                            _con.refreshHome();
                          });
                        },
                        child: Text(
                          'تغيير العنوان',
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      )),
                ),
                _con.carts.isNotEmpty && _con.restaurant != null && Helper.canDelivery(_con.restaurant, carts: _con.carts)
                    ? DeliveryAddressesItemWidget(
                        restaurant: _con.restaurant,
                        paymentMethod: _con.getDeliveryMethod(),
                        address: _con.deliveryAddress,
                        onPressed: (Address _address) {
                          if (_con.deliveryAddress.id == null || _con.deliveryAddress.id == 'null' || _con.deliveryAddress == null) {
                            var bottomSheetController = _con.scaffoldKey?.currentState.showBottomSheet(
                              (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: _con.scaffoldKey),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                              ),
                            );
                            bottomSheetController.closed.then((value) {
                              _con.refreshHome();
                            });
                            _con.scaffoldKey?.currentState?.showSnackBar(
                              SnackBar(
                                content: Text(' يجب ادخال عنوانك للاستمرار ! '),
                              ),
                            );
                          } else {
                            if (_con.carts[0].food.restaurant.minimum_order_total_for_delivery >= _con.subTotal) {
                              _con.scaffoldKey?.currentState?.showSnackBar(SnackBar(
                                content: Text('للتوصيل بجب أن يكون اجمالي الطلب اكثر من : ' + _con.carts[0].food.restaurant.minimum_order_total_for_delivery.toString() + ' دينار '),
                              ));
                            } else {
                              _con.toggleDelivery();
                            }
                          }
                        },
                        onLongPress: (Address _address) {
                          var bottomSheetController = _con.scaffoldKey?.currentState.showBottomSheet(
                            (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: _con.scaffoldKey),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                            ),
                          );
                          bottomSheetController.closed.then((value) {
                            _con.refreshHome();
                          });
                        },
                      )
                    : NotDeliverableAddressesItemWidget(restaurant: _con.restaurant)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10, top: 10),
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                leading: Icon(
                  Icons.domain,
                  color: Theme.of(context).hintColor,
                ),
                title: Text(
                  S.of(context).pickup,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline4,
                ),
                subtitle: Text(
                  S
                      .of(context)
                      .pickup_your_food_from_the_restaurant,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme
                      .of(context)
                      .textTheme
                      .caption,
                ),
              ),
            ),
            PickUpMethodItem(
                paymentMethod: _con.getPickUpMethod(),
                onPressed: (paymentMethod) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Wrap(
                          spacing: 10,
                          children: <Widget>[
                            Icon(Icons.report, color: Colors.orange),
                            Text(
                              S
                                  .of(context)
                                  .confirmation,
                              style: TextStyle(color: Colors.orange),
                            ),
                          ],
                        ),
                        content: Text('لقد قمت باختيار استلام طلبيتك من المطعم. عندما تجهز الطلبية يتوجب عليك الذهاب الى المطعم لاستلامها!'),
                        contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
                        actions: <Widget>[
                          FlatButton(
                            child: new Text(
                              'حسنا',
                              style: TextStyle(color: Theme
                                  .of(context)
                                  .hintColor),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                  print(_con.carts[0].food.restaurant.available_for_pickup.toString());
                  if (_con.carts[0].food.restaurant.available_for_pickup) {
                    if (_con.deliveryAddress.id == null || _con.deliveryAddress.id == 'null' || _con.deliveryAddress == null) {
                      var bottomSheetController = _con.scaffoldKey?.currentState.showBottomSheet(
                            (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: _con.scaffoldKey),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                        ),
                      );
                      bottomSheetController.closed.then((value) {
                        _con.refreshHome();
                      });
                      _con.scaffoldKey?.currentState?.showSnackBar(
                        SnackBar(
                          content: Text(' يجب ادخال عنوانك للاستمرار ! '),
                        ),
                      );
                    } else {
                      _con.togglePickUp();
                    }
                  } else {
                    _con.scaffoldKey?.currentState?.showSnackBar(
                      SnackBar(
                        content: Text(' الاستلام مغلق من قبل المطعم ! '),
                      ),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
