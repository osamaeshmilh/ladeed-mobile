import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/route_argument.dart';

class ReviewSubmitWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  ReviewSubmitWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _ReviewSubmitWidgetState createState() => _ReviewSubmitWidgetState();
}

class _ReviewSubmitWidgetState extends StateMVC<ReviewSubmitWidget> {
  var reviewId;

  _ReviewSubmitWidgetState() : super() {}

  @override
  void initState() {
    // route param contains the payment method
    reviewId = widget.routeArgument.param;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).hintColor,
          ),
          centerTitle: true,
          title: Text(
            S.of(context).confirmation,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 200,
                      width: 200,
                      child: reviewId != null && reviewId != 'null'
                          ? FlareActor("assets/img/checked.flr", alignment: Alignment.center, fit: BoxFit.contain, animation: "Untitled")
                          : FlareActor("assets/img/cancel_icon.flr", alignment: Alignment.center, fit: BoxFit.contain, animation: "Error"),
                    ),
                  ),
                  SizedBox(height: 15),
                  Opacity(
                    opacity: 0.4,
                    child: reviewId != null && reviewId != 'null'
                        ? Text(
                            S.of(context).review_submitted,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
                          )
                        : Text(
                            S.of(context).review_submit_fail,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
                          ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                height: 150,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                    boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, -2), blurRadius: 5.0)]),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width - 40,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed('/Details',
                                arguments: RouteArgument(
                                  id: '0',
                                  param: widget.routeArgument.id,
                                  heroTag: widget.routeArgument.id + " ",
                                ));
                          },
                          padding: EdgeInsets.symmetric(vertical: 14),
                          color: Theme.of(context).accentColor,
                          shape: StadiumBorder(),
                          child: Text(
                            S.of(context).back,
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Theme.of(context).primaryColor),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
