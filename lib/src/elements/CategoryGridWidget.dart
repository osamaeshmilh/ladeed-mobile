import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';
import 'package:food_delivery_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../models/category.dart';

class CategoryGridWidget extends StatefulWidget {
  final List<Category> categories;

  const CategoryGridWidget({
    Key key,
    this.categories,
  }) : super(key: key);

  @override
  _CategoryGridWidget createState() => _CategoryGridWidget();
}

class _CategoryGridWidget extends StateMVC<CategoryGridWidget> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
      primary: false,
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 15),
      crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 4,
      itemCount: widget.categories.length,
      itemBuilder: (BuildContext context, int index) {
        Category category = widget.categories.elementAt(index);
        return InkWell(
          onTap: () {
            Navigator.of(context).pushNamed('/Category', arguments: RouteArgument(id: category.id));
          },
          child: Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(6),
                padding: EdgeInsets.only(top: 10),
                alignment: AlignmentDirectional.topCenter,
                width: double.infinity,
                height: 120,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)],
                    color: Theme.of(context).primaryColor),
                child: Hero(
                  tag: category.id,
                  child: category.image.url.toLowerCase().endsWith('.svg')
                      ? Container(
                          height: 70,
                          width: 220,
                          child: SvgPicture.network(
                            category.image.url,
                            color: Theme.of(context).accentColor,
                          ),
                        )
                      : ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          child: CachedNetworkImage(
                            height: 110,
                            width: 220,
                            fit: BoxFit.cover,
                            imageUrl: category.image.url,
                            placeholder: (context, url) => Image.asset(
                              'assets/img/loading.gif',
                              fit: BoxFit.cover,
                              height: 110,
                              width: 220,
                            ),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                          ),
                        ),
                ),
              ),
              Positioned(
                right: -50,
                bottom: -100,
                child: Container(
                  width: 220,
                  height: 220,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(150),
                  ),
                ),
              ),
              Positioned(
                left: -30,
                top: -60,
                child: Container(
                  width: 120,
                  height: 120,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.12),
                    borderRadius: BorderRadius.circular(150),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 90, bottom: 10),
                width: 140,
                height: 30,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      category.name,
                      style: Theme.of(context).textTheme.display1.merge(TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                      maxLines: 1,
                      softWrap: false,
                      overflow: TextOverflow.fade,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
//                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(index % 2 == 0 ? 1 : 2),
      staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
      mainAxisSpacing: 15.0,
      crossAxisSpacing: 15.0,
    );
  }
}
