import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/controllers/delivery_pickup_controller.dart';
import 'package:food_delivery_app/src/models/zone.dart';

import '../../generated/l10n.dart';
import '../helpers/helper.dart';

class CheckoutBottomDetailsWidget extends StatelessWidget {
  const CheckoutBottomDetailsWidget({
    Key key,
    @required DeliveryPickupController con,
  })  : _con = con,
        super(key: key);

  final DeliveryPickupController _con;

  @override
  Widget build(BuildContext context) {
    return _con.carts.isEmpty
        ? SizedBox(height: 0)
        : Container(
            height: 200,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                      color: Theme.of(context).focusColor.withOpacity(0.15),
                      offset: Offset(0, -2),
                      blurRadius: 5.0)
                ]),
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 40,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          S.of(context).subtotal,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Helper.getPrice(_con.subTotal, context,
                          style: Theme.of(context).textTheme.subtitle1)
                    ],
                  ),
                  SizedBox(height: 5),
                  (_con.is_delivery != null && _con.is_delivery)
                      ? Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                S.of(context).delivery_fee,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                            if (Helper.canDelivery(_con.carts[0].food.restaurant, carts: _con.carts))
                              if (_con.carts[0].food.restaurant.delivery_price_type == 'fixed')
                                Helper.getPrice(
                                    _con.carts[0].food.restaurant.deliveryFee, context,
                                    style:
                                        Theme.of(context).textTheme.subtitle1)
                              else if (_con.carts[0].food.restaurant.delivery_price_type == 'distance' &&
                                  _con.restaurant != null)
                                Helper.getDistancePrice(_con.restaurant.deliveryFee,
                                    Helper.getDistanceKmDouble(_con.restaurant.distance), context,
                                    style:
                                        Theme.of(context).textTheme.subtitle1)
                              else if (_con.carts[0].food.restaurant.delivery_price_type ==
                                  'zone')
                                Helper.getZonePrice(
                                    _con.restaurant.deliveryFee, Zone(), context,
                                    style: Theme.of(context).textTheme.subtitle1)
                              else
                                Helper.getPrice(0, context, style: Theme.of(context).textTheme.subtitle1)
                          ],
                        )
                      : Container(),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          '${S.of(context).tax} (${_con.carts[0].food.restaurant.defaultTax}%)',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Helper.getPrice(_con.taxAmount, context,
                          style: Theme.of(context).textTheme.subtitle1)
                    ],
                  ),
                  SizedBox(height: 10),
                  Stack(
                    fit: StackFit.loose,
                    alignment: AlignmentDirectional.centerEnd,
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: FlatButton(
                          onPressed: () {
                            _con.goCheckout(context);
                          },
                          disabledColor:
                              Theme.of(context).focusColor.withOpacity(0.5),
                          padding: EdgeInsets.symmetric(vertical: 14),
                          color: !_con.carts[0].food.restaurant.closed
                              ? Theme.of(context).accentColor
                              : Theme.of(context).focusColor.withOpacity(0.5),
                          shape: StadiumBorder(),
                          child: Text(
                            S.of(context).checkout,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor)),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Helper.getPrice(
                          (_con.is_delivery != null && _con.is_delivery)
                              ? _con.total
                              : _con.total - _con.deliveryFee,
                          context,
                          style: Theme.of(context).textTheme.headline4.merge(
                              TextStyle(color: Theme.of(context).primaryColor)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          );
  }
}
