import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/helpers/ladeed_icons_icons.dart';

import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../models/restaurant.dart';
import '../repository/settings_repository.dart';

// ignore: must_be_immutable
class CardWidget extends StatelessWidget {
  Restaurant restaurant;
  String heroTag;

  CardWidget({Key key, this.restaurant, this.heroTag}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 280,
      margin: EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 12),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          // Image of the card
          Stack(
            fit: StackFit.loose,
            alignment: AlignmentDirectional.bottomStart,
            children: <Widget>[
              Hero(
                tag: this.heroTag + restaurant.id,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                  child: CachedNetworkImage(
                    height: 120,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    imageUrl: restaurant.image.url,
                    placeholder: (context, url) => Image.asset(
                      'assets/img/loading.gif',
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 120,
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                    padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                    decoration: BoxDecoration(color: restaurant.closed ? Colors.grey : Colors.green, borderRadius: BorderRadius.circular(24)),
                    child: restaurant.closed
                        ? Text(
                            S.of(context).closed,
                            style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                          )
                        : Text(
                            S.of(context).open,
                            style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                          ),
                  ),
                  Helper.canDelivery(restaurant)
                      ? Container(
                          margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                          padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                          decoration: BoxDecoration(color: Helper.canDelivery(restaurant) ? Colors.green : Colors.transparent, borderRadius: BorderRadius.circular(24)),
                          child: Helper.canDelivery(restaurant)
                              ? Text(
                                  S.of(context).delivery,
                                  style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                                )
                              : Text(''),
                        )
                      : Text(''),
                  Helper.canDelivery(restaurant) ? SizedBox(width: 10) : SizedBox(width: 0),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                    padding: EdgeInsets.only(right: 12, left: 12, bottom: 5, top: 2),
                    decoration: BoxDecoration(color: restaurant.available_for_pickup ? Colors.green : Colors.transparent, borderRadius: BorderRadius.circular(24)),
                    child: restaurant.available_for_pickup
                        ? Text(
                            'استلام',
                            style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                          )
                        : Text(''),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            restaurant.name,
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("(" + restaurant.rate_count.toString() + ")", style: Theme.of(context).textTheme.caption),
                                Text(" " + getRatingText(context) + " ", style: Theme.of(context).textTheme.caption.merge(TextStyle(color: getRatingColor(), fontWeight: FontWeight.bold))),
                                Text(restaurant.rate, style: Theme.of(context).textTheme.caption.merge(TextStyle(color: getRatingColor(), fontWeight: FontWeight.bold))),
                                SizedBox(
                                  width: 3,
                                ),
                                Icon(
                                  LadeedIcons.star__1_,
                                  color: getRatingColor(),
                                  size: 16,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      restaurant.distance > 0
                          ? Text(
                              Helper.getDistance(restaurant.distance, Helper.of(context).trans(setting.value.distanceUnit)) + " · " + restaurant.address,
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: Theme.of(context).textTheme.caption,
                            )
                          : Text(
                              restaurant.address,
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: Theme.of(context).textTheme.caption,
                            ),
                      SizedBox(height: 5)
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  String getRatingText(context) {
    double rate = double.parse(restaurant.rate);
    if (rate == 5.0)
      return S.of(context).ladeed;
    else if (rate >= 4.5 && rate <= 4.9)
      return S
          .of(context)
          .excellent;
    else if (rate >= 4.0 && rate < 4.5)
      return S
          .of(context)
          .very_good;
    else if (rate >= 3.5 && rate < 4.0)
      return S
          .of(context)
          .good;
    else if (rate >= 2.5 && rate < 3.5)
      return S
          .of(context)
          .ok;
    else if (rate >= 2.0 && rate < 2.5)
      return S
          .of(context)
          .poor;
    else
      return "";
  }

  Color getRatingColor() {
    double rate = double.parse(restaurant.rate);
    if (rate == 5.0)
      return Colors.green;
    else if (rate >= 4.5 && rate <= 4.9)
      return Colors.green[400];
    else if (rate >= 4.0 && rate < 4.5)
      return Colors.lightGreen[400];
    else if (rate >= 3.5 && rate < 4.0)
      return Colors.yellow[700];
    else if (rate >= 2.5 && rate < 3.5)
      return Colors.orange;
    else if (rate >= 2.0 && rate < 2.5)
      return Colors.red;
    else
      return Colors.grey;
  }
}
