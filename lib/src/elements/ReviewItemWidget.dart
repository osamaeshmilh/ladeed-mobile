import 'package:avatar_letter/avatar_letter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../helpers/helper.dart';
import '../helpers/ladeed_icons_icons.dart';
import '../models/review.dart';
import '../models/route_argument.dart';

// ignore: must_be_immutable
class ReviewItemWidget extends StatelessWidget {
  Review review;

  ReviewItemWidget({Key key, this.review}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('/ReviewDetails', arguments: RouteArgument(id: review.id, heroTag: "review_details" + review.id));
      },
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 10,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              review.user.id != 'null'
                  ? review.user.image.id != null
                      ? ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(300)),
                          child: CachedNetworkImage(
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                            imageUrl: review.user.image?.url,
                            placeholder: (context, url) => Image.asset(
                              'assets/img/loading.gif',
                              fit: BoxFit.cover,
                              height: 50,
                              width: 50,
                            ),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                          ),
                        )
                      : AvatarLetter(
                          size: 50,
                          backgroundColor: Colors.deepOrangeAccent,
                          textColor: Colors.white,
                          fontSize: 22,
                          upperCase: true,
                          numberLetters: 2,
                          letterType: LetterType.Circular,
                          text: review.user.name,
                        )
                  : SizedBox(),
              SizedBox(width: 15),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            review.user.name.split(' ').length > 1
                                ? review.user.name.split(' ')[0] + " " + review.user.name.split(' ')[1].substring(0, 1).toUpperCase()
                                : review.user.name.split(' ')[0],
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.headline6.merge(TextStyle(color: Theme.of(context).hintColor)),
                          ),
                        ),
                        SizedBox(
                          height: 32,
                          child: Chip(
                            padding: EdgeInsets.all(0),
                            label: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(review.averageRating, style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColor))),
                                SizedBox(
                                  width: 4,
                                ),
                                Icon(
                                  LadeedIcons.star__1_,
                                  color: Theme.of(context).primaryColor,
                                  size: 14,
                                ),
                              ],
                            ),
                            backgroundColor: Theme.of(context).accentColor.withOpacity(0.9),
                            shape: StadiumBorder(),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      Helper.skipHtml(review.review),
                      style: Theme.of(context).textTheme.bodyText2,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      maxLines: 3,
                    )
                  ],
                ),
              )
            ],
          ),
          Text(
            DateFormat('yyyy-MM-dd', 'en').format(review.createdAt),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
  }
}
