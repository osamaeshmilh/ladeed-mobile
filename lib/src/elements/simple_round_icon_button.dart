import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SimpleRoundIconButton extends StatelessWidget {
  final Color backgroundColor;
  final String label;
  final Color textColor;
  final SvgPicture svgIcon;
  final Alignment iconAlignment;
  final Function onPressed;

  SimpleRoundIconButton(
      {this.backgroundColor = Colors.white,
      this.label = "label",
      this.textColor = Colors.blue,
      this.svgIcon,
      this.iconAlignment = Alignment.centerLeft,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: OutlineButton(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        textColor: textColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
            side: BorderSide(width: 2)),
        color: backgroundColor,
        borderSide: BorderSide(color: textColor),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(label),
            SizedBox(width: 16.0),
            SizedBox(child: svgIcon, width: 24.0, height: 24.0),
          ],
        ),
        onPressed: onPressed,
      ),
    );
  }
}
