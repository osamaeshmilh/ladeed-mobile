import 'package:flutter/material.dart';
import 'package:food_delivery_app/src/models/aspect.dart';
import 'package:food_delivery_app/src/models/restaurant.dart';
import 'package:food_delivery_app/src/models/route_argument.dart';
import 'package:food_delivery_app/src/repository/restaurant_repository.dart';
import 'package:food_delivery_app/src/repository/settings_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/food.dart';
import '../models/order.dart';
import '../models/order_status.dart';
import '../models/review.dart';
import '../repository/food_repository.dart' as foodRepo;
import '../repository/order_repository.dart';
import '../repository/restaurant_repository.dart' as restaurantRepo;

class ReviewsController extends ControllerMVC {
  Review restaurantReview;
  List<Review> foodsReviews = [];
  Order order;
  Restaurant restaurant;
  List<Food> foodsOfOrder = [];
  List<OrderStatus> orderStatus = <OrderStatus>[];
  GlobalKey<ScaffoldState> scaffoldKey;
  List<Review> latestReviews = <Review>[];

  Review review;
  List<Review> reviewHistory = <Review>[];

  ReviewsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.restaurantReview = new Review.init("0");
    restaurantReview.aspects = List<Aspect>();
    restaurantReview.aspects.add(Aspect.fromJSON({'identifier': 1, 'name': 'الطعم', 'rate': '0'}));
    restaurantReview.aspects.add(Aspect.fromJSON({'identifier': 2, 'name': 'السعر', 'rate': '0'}));
    restaurantReview.aspects.add(Aspect.fromJSON({'identifier': 3, 'name': 'سرعة التجهيز', 'rate': '0'}));
    restaurantReview.aspects.add(Aspect.fromJSON({'identifier': 4, 'name': 'المعاملة', 'rate': '0'}));
  }

  void listenForOrder({String orderId, String message}) async {
    final Stream<Order> stream = await getOrder(orderId);
    stream.listen((Order _order) {
      setState(() {
        order = _order;
        foodsReviews = List.generate(order.foodOrders.length, (_) => new Review.init("0"));
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      getFoodsOfOrder();
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void addFoodReview(Review _review, Food _food) async {
    foodRepo.addFoodReview(_review, _food).then((value) {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).the_food_has_been_rated_successfully),
      ));
    });
  }

  void addRestaurantReview(Review _review) async {
    restaurantRepo.addRestaurantReview(_review, this.order.foodOrders[0].food.restaurant).then((value) {
      refreshOrder();
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).the_restaurant_has_been_rated_successfully),
      ));
    });
  }

  Future<void> refreshOrder() async {
    listenForOrder(orderId: order.id, message: S.of(context).reviews_refreshed_successfully);
  }

  void getFoodsOfOrder() {
    this.order.foodOrders.forEach((_foodOrder) {
      if (!foodsOfOrder.contains(_foodOrder.food)) {
        foodsOfOrder.add(_foodOrder.food);
      }
    });
  }

  void listenForRestaurant({String restaurantId, String message}) async {
    try {
      final Restaurant restaurant = await getRestaurant(restaurantId, deliveryAddress.value);
      this.restaurant = restaurant;
    } catch (e) {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(message),
      ));
    }
  }

  void addRestaurantReviewNoOrder(Review _review) async {
    restaurantRepo.addRestaurantReview(_review, this.restaurant).then((review) {
      refreshRestaurant();
      refreshLatestReviews();
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).the_restaurant_has_been_rated_successfully),
      ));
      Navigator.of(context).pushReplacementNamed('/ReviewSubmit', arguments: RouteArgument(id: this.restaurant.id, heroTag: '', param: review.id));
    });
  }

  Future<void> listenForLatestReviews() async {
    final Stream<Review> stream = await getRecentReviews();
    stream.listen((Review _review) {
      setState(() => latestReviews.add(_review));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> refreshRestaurant() async {
    listenForRestaurant(restaurantId: restaurant.id, message: S.of(context).reviews_refreshed_successfully);
  }

  Future<void> refreshLatestReviews() async {
    latestReviews.clear();
    listenForLatestReviews();
  }

  Future<void> listenForReviewDetails(reviewId, {String message}) async {
    final Stream<Review> stream = await getReview(reviewId);
    stream.listen((Review _review) {
      setState(() {
        review = _review;
      });
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForReviewHistory(reviewId, {String message}) async {
    final Stream<Review> stream = await getReviewHistory(reviewId);
    stream.listen((Review _review) {
      setState(() {
        reviewHistory.add(_review);
      });
    }, onError: (a) {}, onDone: () {});
  }
}
