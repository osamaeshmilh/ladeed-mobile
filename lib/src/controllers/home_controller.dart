import 'package:flutter/cupertino.dart';
import 'package:food_delivery_app/src/models/slide.dart';
import 'package:food_delivery_app/src/repository/slider_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../helpers/helper.dart';
import '../models/category.dart';
import '../models/food.dart';
import '../models/restaurant.dart';
import '../models/review.dart';
import '../repository/category_repository.dart';
import '../repository/food_repository.dart';
import '../repository/restaurant_repository.dart';
import '../repository/settings_repository.dart';

class HomeController extends ControllerMVC {
  List<Category> categories = <Category>[];
  List<Restaurant> nearRestaurants = <Restaurant>[];
  List<Restaurant> popularRestaurants = <Restaurant>[];
  List<Restaurant> mostReviewedRestaurants = <Restaurant>[];
  List<Restaurant> recentRestaurants = <Restaurant>[];
  List<Restaurant> recentReviewedRestaurants = <Restaurant>[];
  List<Review> recentReviews = <Review>[];
  List<Food> trendingFoods = <Food>[];
  List<Slide> slides = <Slide>[];

  var location = false;

  HomeController() {
    listenForNearRestaurants();
    listenForSlides();
    listenForCategories();
    listenForRecentReviewedRestaurants();
    //listenForPopularRestaurants();
    //listenForRecentReviews();
  }

  Future<void> listenForSlides() async {
    final Stream<Slide> stream = await getSlides();
    stream.listen((Slide _slide) {
      setState(() => slides.add(_slide));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> listenForCategories() async {
    final Stream<Category> stream = await getCategories();
    stream.listen((Category _category) {
      setState(() => categories.add(_category));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> listenForNearRestaurants() async {
    final Stream<Restaurant> stream = await getNearRestaurants(deliveryAddress.value, deliveryAddress.value);
    stream.listen((Restaurant _restaurant) {
      setState(() => nearRestaurants.add(_restaurant));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForMostReviewedRestaurantsRestaurants() async {
    final Stream<Restaurant> stream = await getMostReviewedRestaurants(deliveryAddress.value);
    stream.listen((Restaurant _restaurant) {
      setState(() => mostReviewedRestaurants.add(_restaurant));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForPopularRestaurants() async {
    final Stream<Restaurant> stream = await getPopularRestaurants(deliveryAddress.value);
    stream.listen((Restaurant _restaurant) {
      setState(() => popularRestaurants.add(_restaurant));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForRecentRestaurants() async {
    final Stream<Restaurant> stream = await getRecentRestaurants(deliveryAddress.value);
    stream.listen((Restaurant _restaurant) {
      setState(() => recentRestaurants.add(_restaurant));
    }, onError: (a) {}, onDone: () {});
  }

  void listenForRecentReviewedRestaurants() async {
    final Stream<Restaurant> stream = await getRecentReviewedRestaurants(deliveryAddress.value);
    stream.listen((Restaurant _restaurant) {
      setState(() => recentReviewedRestaurants.add(_restaurant));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForRecentReviews() async {
    final Stream<Review> stream = await getRecentReviews();
    stream.listen((Review _review) {
      setState(() => recentReviews.add(_review));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForTrendingFoods() async {
    final Stream<Food> stream = await getTrendingFoods(deliveryAddress.value);
    stream.listen((Food _food) {
      setState(() => trendingFoods.add(_food));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  void requestForCurrentLocation(BuildContext context) {
    OverlayEntry loader = Helper.overlayLoader(context);
    Overlay.of(context).insert(loader);
    setCurrentLocation().then((_address) async {
      deliveryAddress.value = _address;
      await refreshHome();
      loader.remove();
    }).catchError((e) {
      loader.remove();
    });
  }

  void requestForCurrentLocationNoLoader(BuildContext context) {
    setCurrentLocation().then((_address) async {
      deliveryAddress.value = _address;
      await refreshHome();
    }).catchError((e) {});
  }

  void requestForCurrentLocationOnStart(BuildContext context) {
    setCurrentLocation().then((_address) async {
      location = true;
      deliveryAddress.value = _address;
      await refreshHome();
    }).catchError((e) {});
  }

  Future<void> refreshHome() async {
    setState(() {
      categories = <Category>[];
      nearRestaurants = <Restaurant>[];
      recentReviewedRestaurants = <Restaurant>[];
      //recentReviews = <Review>[];
    });
    await listenForNearRestaurants();
    await listenForCategories();
    await listenForRecentReviewedRestaurants();
  }
}
