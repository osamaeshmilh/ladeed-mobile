import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../models/cart.dart';
import '../models/coupon.dart';
import '../models/credit_card.dart';
import '../models/distance_price.dart';
import '../models/food_order.dart';
import '../models/order.dart';
import '../models/order_status.dart';
import '../models/payment.dart';
import '../models/zone.dart';
import '../models/zone_price.dart';
import '../repository/order_repository.dart' as orderRepo;
import '../repository/settings_repository.dart' as settingRepo;
import '../repository/user_repository.dart' as userRepo;
import 'cart_controller.dart';

class CheckoutController extends CartController {
  bool is_delivery;
  Payment payment;
  double taxAmount = 0.0;
  double deliveryFee = 0.0;
  double subTotal = 0.0;
  double total = 0.0;
  CreditCard creditCard = new CreditCard();
  bool loading = true;
  bool ok = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  CheckoutController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForCreditCard();
  }

  void listenForCreditCard() async {
    creditCard = await userRepo.getCreditCard();
    setState(() {});
  }

  @override
  void onLoadingCartDone() {
    if (payment != null && is_delivery != null && restaurant != null) addOrder(carts, restaurant);
    super.onLoadingCartDone();
  }

  void addOrder(List<Cart> carts, restaurant) async {
    Order _order = new Order();
    _order.foodOrders = new List<FoodOrder>();
    _order.tax = carts[0].food.restaurant.defaultTax;

    if (carts[0].food.restaurant.delivery_price_type == 'fixed') {
      _order.deliveryFee = carts[0].food.restaurant.deliveryFee.toDouble();
    } else if (carts[0].food.restaurant.delivery_price_type == 'distance')
      _order.deliveryFee = getDistancePrice(restaurant.deliveryFee, Helper.getDistanceKmDouble(restaurant.distance));
    else if (carts[0].food.restaurant.delivery_price_type == 'zone')
      _order.deliveryFee = getZonePrice(restaurant.deliveryFee, Zone());
    else
      _order.deliveryFee = carts[0].food.restaurant.deliveryFee;

    _order.deliveryFee = payment.method == 'Pay on Pickup' ? 0 : _order.deliveryFee;
    OrderStatus _orderStatus = new OrderStatus();
    _orderStatus.id = '1'; // TODO default order status Id
    _order.orderStatus = _orderStatus;
    _order.deliveryAddress = settingRepo.deliveryAddress.value;
    _order.is_delivery = is_delivery;
    carts.forEach((_cart) {
      FoodOrder _foodOrder = new FoodOrder();
      _foodOrder.quantity = _cart.quantity;
      _foodOrder.price = _cart.food.price;
      _foodOrder.food = _cart.food;
      _foodOrder.extras = _cart.extras;
      _order.foodOrders.add(_foodOrder);
    });
    orderRepo.addOrder(_order, this.payment).then((value) async {
      settingRepo.coupon.value = new Coupon.fromJSON({});
      return value;
    }).then((value) {
      if (value is Order) {
        setState(() {
          ok = true;
          loading = false;
        });
      } else {
        setState(() {
          ok = false;
          loading = false;
        });
      }
    }).catchError((onError) {
      setState(() {
        ok = false;
        loading = false;
      });
    });
  }

  void updateCreditCard(CreditCard creditCard) {
    userRepo.setCreditCard(creditCard).then((value) {
      setState(() {});
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).payment_card_updated_successfully),
      ));
    });
  }

  static getDistancePrice(List distancePriceList, double distance) {
    List<DistancePrice> distancePrices;
    distancePrices = List.from(distancePriceList).map((element) => DistancePrice.fromJSON(element)).toSet().toList();

    var myPrice = 0.0;
    distancePrices.forEach((distancePrice) {
      if (distance >= distancePrice.from && distance <= distancePrice.to) {
        myPrice = distancePrice.price;
      }
    });

    try {
      return myPrice;
    } catch (e) {
      return 0.0;
    }
  }

  static getZonePrice(List zonePricesList, Zone selectedZone) {
    List<ZonePrice> zonePrices;
    zonePrices = List.from(zonePricesList).map((element) => ZonePrice.fromJSON(element)).toSet().toList();
    var myPrice = 0.0;
    zonePrices.forEach((zonePrice) {
      if (zonePrice.zone.id == selectedZone.id) {
        myPrice = zonePrice.price;
      }
    });
    try {
      return myPrice;
    } catch (e) {
      return 0.0;
    }
  }
}
